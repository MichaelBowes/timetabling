package wcttt.gui.controller;

import java.util.HashMap;
import java.util.Map;

import javafx.application.HostServices;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.input.MouseEvent;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.stage.Stage;
import wcttt.gui.model.Model;
import wcttt.lib.model.Period;
import wcttt.lib.model.Teacher;

/**
 * 
 * Controller for the window displaying the Teacher-Period Matrix, which
 * shows the relation of each teacher's preferences to the available periods.
 * 
 * @author Michael Bowes
 *
 */

public class ViewTeacherPeriodController extends Controller {

	@FXML
	private ScrollPane scroll;

	@FXML
	private GridPane matrix;

	@FXML
	private BorderPane rootPane;

	@FXML
	private TextField xAxisName;

	@FXML
	private TextField yAxisName;

	private Map<Integer, String> days = new HashMap<Integer, String>();
	private Map<Integer, String> timeslots = new HashMap<Integer, String>();

	@FXML
	protected void initialize() {

		scroll = new ScrollPane(matrix);
		scroll.setVbarPolicy(ScrollBarPolicy.ALWAYS);
		scroll.setHbarPolicy(ScrollBarPolicy.ALWAYS);
		rootPane.setCenter(scroll);

		days.put(1, "Mo");
		days.put(2, "Di");
		days.put(3, "Mi");
		days.put(4, "Do");
		days.put(5, "Fr");
		timeslots.put(1, "8:15 - 9:45");
		timeslots.put(2, "10:15 - 11:45");
		timeslots.put(3, "12:15 - 13:45");
		timeslots.put(4, "14:15 - 15:45");
		timeslots.put(5, "16:15 - 17:45");
		timeslots.put(6, "18:15 - 19:45");
	}

	/**
	 * Generates the teacher-period matrix.
	 */
	private void generateMatrix() {

		ObservableList<Teacher> teachers = getModel().getTeachers();

		int x = 0;
		for (Teacher t : teachers) {

			double s = 1 / getModel().getTeachers().size();
			TextField teacherTextField = new TextField(t.getName());
			teacherTextField.setEditable(false);
			teacherTextField.maxWidth(200);
			teacherTextField.setMouseTransparent(true);
			ColumnConstraints columnConstraint = new ColumnConstraints();
			columnConstraint.setPercentWidth(s);
			RowConstraints col2 = new RowConstraints();
			col2.setPercentHeight(s);
			matrix.getColumnConstraints().addAll(columnConstraint);
			matrix.getRowConstraints().addAll(col2);
			matrix.add(teacherTextField, x + 1, 0);
			int y = 0;
			for (Period p : getModel().getPeriods()) {
				TextField periodTextField = new TextField(days.get(p.getDay()) + timeslots.get(p.getTimeSlot()));
				periodTextField.setEditable(false);
				periodTextField.maxWidth(200);
				periodTextField.setMouseTransparent(true);
				matrix.add(periodTextField, 0, y + 1);

				int conflictType;
				if (t.getUnavailablePeriods().contains(p)) {
					conflictType = 2;
				} else if (t.getUnfavorablePeriods().contains(p)) {
					conflictType = 1;
				} else {
					conflictType = 0;
				}
				Circle circle = drawCircle(conflictType);
				Tooltip.install(circle, createConflictTooltip(conflictType));
				matrix.add(circle, x + 1, y + 1);
				GridPane.setHalignment(circle, HPos.CENTER);
				y++;
			}

			x++;
		}
	}

	/**
	 * Returns a line for the grid with the given start and end coordinates.
	 *
	 * @param x1 the x value of the start coordinate.
	 * @param y1 the y value of the start coordinate.
	 * @param x2 the x value of the end coordinate.
	 * @param y2 the y value of the end coordinate.
	 * @return the line.
	 */
	private Line getGridLine(double x1, double y1, double x2, double y2) {
		Line line = new Line(x1, y1, x2, y2);
		line.setStroke(Color.LIGHTGRAY);
		line.getStrokeDashArray().addAll(5d);
		return line;
	}

	/**
	 * Draws a {@link Circle} with attached {@link Tooltip} for the specific conflict.
	 * 
	 * @param conflictType
	 * @return
	 */
	private Circle drawCircle(int conflictType) {
		Circle circle = new Circle();
		circle.setFill(getConflictColor(conflictType));
		circle.setRadius(25);
		circle.setCenterX(0);
		circle.setCenterY(0);
		circle.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				// event?
			}
		});

		return circle;
	}

	/**
	 * Creates {@link Tooltip} for hover over the conflict circles.
	 * 
	 * @param conflictType
	 * @return
	 */
	private Tooltip createConflictTooltip(int conflictType) {
		Tooltip tip = null;
		switch (conflictType) {
		case 0:
			tip = new Tooltip("No Conflict");
			break;
		case 1:
			tip = new Tooltip("Unfavorable");
			break;
		case 2:
			tip = new Tooltip("Unavailable");
			break;
		}

		return tip;
	}

	/**
	 * 
	 * Chooses either white, yellow or red as Paint color depending on whether the
	 * conflict is non-existant, soft or hard constraint.
	 * 
	 * @param conflict
	 * @return
	 */
	private Paint getConflictColor(int conflictType) {
		Color color = null;
		switch (conflictType) {
		case 0:
			color = Color.WHITE;
			break;
		case 1:
			color = Color.YELLOW;
			break;
		case 2:
			color = Color.RED;
			break;
		}
		return color;
	}
	
	@Override
	public void setup(Stage stage, HostServices hostServices, MainController mainController, Model model) {
		super.setup(stage, hostServices, mainController, model);
		generateMatrix();
	}

}
