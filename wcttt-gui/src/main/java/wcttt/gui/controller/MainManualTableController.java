/*
 * WCT³ (WIAI Course Timetabling Tool) is a software that strives to automate
 * the timetabling process at the WIAI faculty of the University of Bamberg.
 *
 * WCT³-GUI comprises functionality to view generated timetables, edit semester
 * data and to generate new timetables.
 *
 * Copyright (C) 2018 Nicolas Gross
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package wcttt.gui.controller;

import wcttt.gui.model.Model;
import wcttt.gui.model.SelectedSession;
import wcttt.gui.model.SelectedSlot;
import wcttt.gui.model.Selection;
import wcttt.lib.algorithms.WctttAlgorithmException;
import wcttt.lib.model.*;
import wcttt.lib.util.TimetableUtil;
import wcttt.lib.util.Tuple;
import javafx.application.HostServices;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Controller for the tables that contain the selected timetable.
 * 
 * @author Michael Bowes
 * @author Nicolas Bruch
 */
public class MainManualTableController extends Controller {

	@FXML
	private ListView<Session> sessionListView;
	@FXML
	private VBox timetableDaysVBox;
	@FXML
	private Button saveButton;
	@FXML
	private Button swapButton; // swaps 2 assignments
	@FXML
	private TextArea assignmentField; // Shows all selected assignments
	@FXML
	private TextArea violationField; // Shows all occurring violations
	@FXML
	private Label penaltyChangeField;

	private List<Selection> selection = new ArrayList<Selection>();
	private List<Tuple<ConstraintType, String>> violations = new ArrayList<Tuple<ConstraintType, String>>();
	private List<TableView<TimetablePeriod>> timetableDays = new ArrayList<>();
	private Timetable timetable;

	private ObservableList<Session> sessions = FXCollections.observableArrayList();

	private TimetableAssignment assignment1;
	private Room room1;
	private TimetableDay day1;
	private TimetablePeriod period1;

	private TimetableAssignment assignment2;
	private Room room2;
	private TimetableDay day2;
	private TimetablePeriod period2;

	@Override
	public void setup(Stage stage, HostServices hostServices, MainController mainController, Model model) {
		super.setup(stage, hostServices, mainController, model);
		assignmentField.setEditable(false);
		violationField.setEditable(false);

		sessionListView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
		sessionListView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Session>() {
			@Override
			public void changed(ObservableValue observable, Session oldValue, Session newValue) {
				if (newValue != null) {
					updateSelection(new SelectedSession(newValue));
					sessionListView.refresh();
				}
			}
		});

		swapButton.setOnAction(event -> {
			if (selection.size() == 2) {
				if (violations.size() == 0) {
					try {
						Selection selection1 = selection.get(0);
						Selection selection2 = selection.get(1);
						if (selection1 instanceof SelectedSlot) {
							if (selection2 instanceof SelectedSlot) {
								TimetableUtil.swapTimeslots(timetable, assignment1, day1, period1, room1, assignment2,
										day2, period2, room2);
								timetableDays.get(day1.getDay() - 1).refresh();
								timetableDays.get(day2.getDay() - 1).refresh();

							} else {
								SelectedSession selectedSession = (SelectedSession) selection2;
								// Put S2 in the slot of S1	
								Session removedSession = TimetableUtil.addSession(selectedSession.getSession(),
										timetable, room1, period1, day1);
								if(removedSession != null) {
									sessions.add(assignment2.getSession());
								}
								timetableDays.get(day1.getDay() - 1).refresh();
								sessions.remove(((SelectedSession) selection2).getSession());
							}
						} else {
							// Put S1 in the slot of S2
							SelectedSession selectedSession = (SelectedSession) selection1;						
							Session removedSession = TimetableUtil.addSession(selectedSession.getSession(),
									timetable, room2, period2, day2);
							
							if(removedSession != null) {
								sessions.add(assignment2.getSession());
							}
							timetableDays.get(day2.getDay() - 1).refresh();							
							sessions.remove(((SelectedSession) selection1).getSession());
						}
					} catch (WctttModelException | WctttAlgorithmException e) {
						Util.errorAlert("An error occured while swapping the assignments", e.getMessage());
					}
				}
			}
		});

		saveButton.setOnAction(event -> {
			if (sessions.size() > 0) {
				Util.informationAlert("Timetable incomplete", "There are still unassigned sessions!");
			} else {
				try {
					getModel().addTimetable(timetable);
					getModel().setChanged(true);
				} catch (WctttModelException e) {
					Util.errorAlert("Could not save timetable", e.getMessage());
					e.printStackTrace();
				}
			}
		});

		Platform.runLater(this::updateGui);
	}

	
	@SuppressWarnings("unchecked")
	private Timetable createTimetable(Semester semester) {
		Timetable timetable = new Timetable();
		try {
			for (int i = 1; i <= semester.getDaysPerWeek(); i++) {
				TimetableDay day = new TimetableDay(i);
				for (int j = 1; j <= semester.getTimeSlotsPerDay(); j++) {
					TimetablePeriod period = new TimetablePeriod(i, j);
					day.addPeriod(period);
				}
				timetable.addDay(day);
			}
		} catch (WctttModelException e) {
			throw new RuntimeException("Could not create new Timetable");
		}
		//Set external sessions
		List<ExternalSession> externalSessions = new ArrayList<ExternalSession>();
		for (Course course : getModel().getCourses()) {
			externalSessions.addAll((List<ExternalSession>)(List<?>)
					course.getLectures().stream()
					.filter(l -> l.hasPreAssignment() && l instanceof ExternalSession)
					.collect(Collectors.toList()));

			externalSessions.addAll((List<ExternalSession>)(List<?>)
					course.getPracticals().stream()
					.filter(p -> p.hasPreAssignment() && p instanceof ExternalSession)
					.collect(Collectors.toList()));
		}
		for(ExternalSession session : externalSessions) {
			Period preAssign = session.getPreAssignment().get();
			for(TimetableDay day : timetable.getDays()) {
				if(preAssign.getDay() == day.getDay()) {
					for(TimetablePeriod period : day.getPeriods()) {
						if(preAssign.getTimeSlot() == period.getTimeSlot()) {
							try {
								period.addAssignment(new TimetableAssignment(session,
										session.getRoom()));
							} catch (WctttModelException e) {
								Util.errorAlert("Could not add external session", e.getMessage());
								e.printStackTrace();
							}
						}
					}
				}			
			}		
		}
		
		return timetable;
	}

	private void updateGui() {
		timetable = createTimetable(getModel().getSemester());
		createSessionListView();
		createTableViews();
		timetableDaysVBox.getChildren().setAll(timetableDays);
		createPeriodColumns();
		createRoomColumns(getModel().getInternalRooms());
		createRoomColumns(getModel().getExternalRooms());
	}

	private String getAssignmentText() {
		String result = "";
		int counter = 0;
		for (Selection selected : selection) {
			if (selected instanceof SelectedSlot) {
				SelectedSlot selectedSlot = (SelectedSlot) selected;
				result += selectedSlot.getDay() + " - " + TimetablePeriod.TIME_SLOT_NAMES[selectedSlot.getPeriod() - 1]
						+ ":" + System.lineSeparator();
				if (selectedSlot.getAssignment() != null) {
					result += selectedSlot.getAssignment().getSession().toString();
				} else {
					result += "Free Slot";
				}
				result += " - " + selectedSlot.getRoom().toString();
			} else {
				SelectedSession selectedSession = (SelectedSession) selected;
				result += selectedSession.getSession() + " (Unassigned)";
			}
			if (counter != 1) {
				result += System.lineSeparator() + System.lineSeparator();
			}
			counter++;
		}
		return result;
	}

	private void createSessionListView() {
		for (Course course : getModel().getCourses()) {
			sessions.addAll(
					course.getLectures().stream()
					.filter(l -> !l.hasPreAssignment() && l instanceof InternalSession)
					.collect(Collectors.toList()));

			sessions.addAll(
					course.getPracticals().stream()
					.filter(p -> !p.hasPreAssignment() && p instanceof InternalSession)
					.collect(Collectors.toList()));
		}
		sessionListView.setItems(sessions);
	}

	private void createTableViews() {
		timetableDays.clear();
		for (int i = 0; i < getModel().getDaysPerWeek(); i++) {
			TableView<TimetablePeriod> tableView = new TableView<>();
			tableView.setPrefWidth(Region.USE_COMPUTED_SIZE);
			tableView.setPrefHeight(196);
			tableView.setEditable(false);
			tableView.getSelectionModel().setCellSelectionEnabled(true);

			TableColumn<TimetablePeriod, String> tableColumn = new TableColumn<>();
			tableColumn.setResizable(false);
			tableColumn.setSortable(false);
			tableColumn.setPrefWidth(100.0);
			tableView.getColumns().add(tableColumn);

			tableView.getFocusModel().focusedCellProperty().addListener(new ChangeListener<TablePosition>() {

				@Override
				public void changed(ObservableValue<? extends TablePosition> observable, TablePosition oldValue,
						TablePosition newValue) {

					int column = newValue.getColumn();
					int row = newValue.getRow();
					if (newValue != null && tableView.getColumns().size() > 0 && column > 0) {
						String roomName = (String) tableView.getColumns().get(column).getText();
						Optional<TimetableAssignment> assignment = tableView.getItems().get(newValue.getRow())
								.getAssignments().stream().filter(x -> x.getRoom().getName().equals(roomName))
								.findFirst();

						String day = (String) tableView.getColumns().get(0).getText();

						SelectedSlot selectedAssignment = null;
						if (assignment.isPresent()) {
							// An assignment was selected
							selectedAssignment = new SelectedSlot(assignment.get(), day, row + 1, roomName);
						} else if (column > 0) {
							// A free slot was selected
							selectedAssignment = new SelectedSlot(null, day, row + 1, roomName);
						}
						updateSelection(selectedAssignment);
					}
				}

			});
			timetableDays.add(tableView);
		}
		for (int i = 0; i < getModel().getDaysPerWeek(); i++) {
			timetableDays.get(i).setItems(timetable.getDays().get(i).getPeriods());
		}
	}

	/**
	 * Checks if two objects have been selected
	 * 
	 * @param newSelection
	 */
	private void updateSelection(Selection newSelection) {
		for(TableView<TimetablePeriod> table : timetableDays) {
			for(TableColumn<TimetablePeriod, ?> column : table.getColumns()) {
				column.setStyle(null);
			}
		}
		if (selection.size() >= 2) {
			selection.clear();
		}
		selection.add(newSelection);
		assignmentField.setText(getAssignmentText());
		if (selection.size() == 2) {
			try {
				showViolations();
			} catch (WctttModelException | WctttAlgorithmException e) {
				Util.errorAlert("An error occured while calculating the violations", e.getMessage());
				e.printStackTrace();
			}
		}
		updateSwapButton();
		for(TableView<TimetablePeriod> table : timetableDays) {
			table.refresh();
		}
		clearSelection();
	}
	
	private void clearSelection() {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				for(TableView<TimetablePeriod> table : timetableDays) {
					table.getSelectionModel().clearSelection();
				}
				sessionListView.getSelectionModel().clearSelection();			
			}			
		});
		
	}

	private void showViolations() throws WctttModelException, WctttAlgorithmException {
		if (selection.size() != 2) {
			throw new RuntimeException("Can't calculate violations if there are not two selections");
		}
		violations.clear();
		setSelectionData();
		double penalty = 0;
		Selection selection1 = selection.get(0);
		Selection selection2 = selection.get(1);
		Semester semester = getModel().getSemester();
		// Check if the selected objects are from the session list or the timetable
		if (selection1 instanceof SelectedSession) {
			// If both are from the session list no swapping is possible
			if (selection2 instanceof SelectedSession) {
				violations.add(new Tuple<ConstraintType, String>(null, "Can't swap two unassigned sessions"));
			} else {
				Tuple<Double, List<Tuple<ConstraintType, String>>> result = TimetableUtil.getAddingViolations(semester,
						timetable, assignment2, ((SelectedSession) selection1).getSession(), day2, period2, room2);
				penalty = result.x;
				violations.addAll(result.y);
			}
		} else if (selection2 instanceof SelectedSession) {
			// If both are from the session list no swapping is possible
			if (selection1 instanceof SelectedSession) {
				violations.add(new Tuple<ConstraintType, String>(null, "Can't swap two unassigned sessions"));
			} else {
				Tuple<Double, List<Tuple<ConstraintType, String>>> result = TimetableUtil.getAddingViolations(semester,
						timetable, assignment1, ((SelectedSession) selection2).getSession(), day1, period1, room1);
				penalty = result.x;
				violations.addAll(result.y);
			}
		} else {
			Tuple<Double, List<Tuple<ConstraintType, String>>> result = TimetableUtil.getViolations(semester, timetable,
					assignment1, day1, period1, room1, assignment2, day2, period2, room2);
			penalty = result.x;
			violations.addAll(result.y);
		}
		setViolationsString(violations);
		setPenaltyText(penalty);
	}

	/**
	 * Sets the text that shows the old and new penalty.
	 * 
	 * @param newValue
	 */
	private void setPenaltyText(double newValue) {
		String text = "";
		text += Util.round(timetable.getSoftConstraintPenalty(), 2);
		text += " -> ";
		text += Util.round(newValue, 2);
		penaltyChangeField.setText(text);
	}

	/**
	 * Sets all needed objects for the calculation of violations and the swapping
	 * process.
	 */
	private void setSelectionData() {
		assignment1 = null;
		assignment2 = null;
		room1 = null;
		room2 = null;
		day1 = null;
		day2 = null;
		period1 = null;
		period2 = null;

		if (selection.get(0) instanceof SelectedSlot) {
			SelectedSlot selectedSlot1 = (SelectedSlot) selection.get(0);
			assignment1 = selectedSlot1.getAssignment();
			room1 = getModel().getSemester().getInternalRooms().stream()
					.filter(r -> r.getName().equals(selectedSlot1.getRoom())).findFirst().orElse(null);

			day1 = timetable.getDays().stream()
					.filter(d -> TimetablePeriod.WEEK_DAY_NAMES[d.getDay() - 1].equals(selectedSlot1.getDay()))
					.findFirst().get();

			period1 = day1.getPeriods().stream().filter(p -> p.getTimeSlot() == selectedSlot1.getPeriod()).findFirst()
					.get();
		}

		// Data for the second selection
		if (selection.get(1) instanceof SelectedSlot) {
			SelectedSlot selectedSlot2 = (SelectedSlot) selection.get(1);
			assignment2 = selectedSlot2.getAssignment();
			room2 = getModel().getSemester().getInternalRooms().stream()
					.filter(r -> r.getName().equals(selectedSlot2.getRoom())).findFirst().orElse(null);

			day2 = timetable.getDays().stream()
					.filter(d -> TimetablePeriod.WEEK_DAY_NAMES[d.getDay() - 1].equals(selectedSlot2.getDay()))
					.findFirst().get();

			period2 = day2.getPeriods().stream().filter(p -> p.getTimeSlot() == selectedSlot2.getPeriod()).findFirst()
					.get();
		}
	}

	private void updateSwapButton() {
		if (this.violations.size() > 0 || this.selection.size() != 2) {
			swapButton.setDisable(true);
		} else {
			swapButton.setDisable(false);
		}
	}

	public void setViolationsString(List<Tuple<ConstraintType, String>> violations) {
		String result = "";
		int counter = 0;
		for (Tuple<ConstraintType, String> violation : violations) {
			if (!result.contains(violation.y)) {
				result += violation.y;
				if (counter < violations.size() - 1) {
					result += System.lineSeparator() + System.lineSeparator();
				}
				counter++;
			}
		}

		violationField.setText(result);
	}

	private void createPeriodColumns() {
		for (int i = 0; i < getModel().getDaysPerWeek(); i++) {
			TableView<TimetablePeriod> tableView = timetableDays.get(i);
			@SuppressWarnings("unchecked")
			TableColumn<TimetablePeriod, String> periodColumn = (TableColumn<TimetablePeriod, String>) tableView
					.getColumns().get(0);
			periodColumn.setText(Period.WEEK_DAY_NAMES[i]);
			periodColumn.setCellValueFactory(
					param -> new SimpleStringProperty(Period.TIME_SLOT_NAMES[param.getValue().getTimeSlot() - 1]));
			periodColumn.setReorderable(false);
		}
	}

	private void createRoomColumns(List<? extends Room> rooms) {
		for (Room room : rooms) {
			for (TableView<TimetablePeriod> tableView : timetableDays) {
				TableColumn<TimetablePeriod, String> tableColumn = new TableColumn<>();
				tableColumn.setText(room.getName());
				tableColumn.setId(room.getId());
				tableColumn.setResizable(false);
				tableColumn.setSortable(false);
				tableColumn.setReorderable(false);
				tableColumn.setPrefWidth(150.0);
				
				tableColumn.setCellValueFactory(new PropertyValueFactory<>("default"));

				tableColumn.setCellValueFactory(param -> {
					for (TimetableAssignment assignment : param.getValue().getAssignments()) {
						if (assignment.getRoom().getId().equals(param.getTableColumn().getId())) {
							return new SimpleStringProperty(assignment.getSession().toString());
						}
					}
					return new SimpleStringProperty("");
				});
				
				tableView.getColumns().add(tableColumn);
			}
		}
	}
}