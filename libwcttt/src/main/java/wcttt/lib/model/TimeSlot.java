package wcttt.lib.model;

public class TimeSlot {

	private TimetableDay day;
	private TimetablePeriod period;
	private Room room;
	private TimetableAssignment assignment;
	
	public TimeSlot(TimetableDay day, TimetablePeriod period, Room room) {
		this.day = day;
		this.period = period;
		this.room = room;
	}
	
	public TimetableDay getDay() {
		return day;
	}

	public void setDay(TimetableDay day) {
		this.day = day;
	}

	public TimetablePeriod getPeriod() {
		return period;
	}

	public void setPeriod(TimetablePeriod period) {
		this.period = period;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public TimetableAssignment getAssignment() {
		return assignment;
	}

	public void setAssignment(TimetableAssignment assignment) {
		this.assignment = assignment;
	}


}
