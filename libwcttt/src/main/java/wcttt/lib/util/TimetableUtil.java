package wcttt.lib.util;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import wcttt.lib.algorithms.WctttAlgorithmException;
import wcttt.lib.model.ConstraintType;
import wcttt.lib.model.Room;
import wcttt.lib.model.Semester;
import wcttt.lib.model.Session;
import wcttt.lib.model.Timetable;
import wcttt.lib.model.TimetableAssignment;
import wcttt.lib.model.TimetableDay;
import wcttt.lib.model.TimetablePeriod;
import wcttt.lib.model.WctttModelException;

/**
 * Contains methods for manipulating {@link Timetable}s.
 * 
 * @author Nicolas Bruch      
 *
 */
public class TimetableUtil {

	/**
	 * Removes an {@link TimetableAssignment} (or two if it is a double session) from a given {@link Timetable}.
	 * @param timetable
	 * @param assignment
	 */
	public static boolean removeSessionAssignments(Timetable timetable, TimetableAssignment assignment) {
		boolean result = false;
		List<Tuple<TimetablePeriod, TimetableAssignment>> removing =
				new LinkedList<Tuple<TimetablePeriod, TimetableAssignment>>();		
		//Selecting all matching assignments
		for (TimetableDay day : timetable.getDays()) {
			for (TimetablePeriod period : day.getPeriods()) {
				for (TimetableAssignment otherAssignment : period.getAssignments()) {
					if (otherAssignment.getSession().equals(assignment.getSession())) {
						removing.add(new Tuple<TimetablePeriod, TimetableAssignment>(period, assignment));
					}
				}
			}
		}
		//Removing the found assignments
		for(int i = 0; i < removing.size(); i++) {
			if(removing.get(i).x.removeAssignment(removing.get(i).y)) {
				result = true;
			};
		}
		return result;
	}

	/**
	 * Checks the violations for hard constraints that would occur if the two were
	 * to be swapped.
	 * 
	 * @param semester    - Semester of the timetable.
	 * @param timetable   - The {@link Timetable} containing the slots.
	 * @param assignment1 - First assignment (first time slot). May be null if the
	 *                    slot is free.
	 * @param day1        - Day of the first time slot.
	 * @param period1     - Period of the first time slot.
	 * @param room1       - Room of the first time slot.
	 * @param assignment2 - Second assignment (second time slot). May be null if the
	 *                    slot is free.
	 * @param day2        - Day of the second time slot.
	 * @param period2     - Period of the second time slot.
	 * @param room2       - Room of the second time slot.
	 * @return A tuple containing the resulting new penalty if the two were swapped
	 *         and a list containing the types of constraint violation (may be null)
	 *         and their description text.
	 * @throws WctttModelException
	 * @throws WctttAlgorithmException
	 */
	public static Tuple<Double, List<Tuple<ConstraintType, String>>> getViolations(Semester semester,
			Timetable timetable, TimetableAssignment assignment1, TimetableDay day1, TimetablePeriod period1,
			Room room1, TimetableAssignment assignment2, TimetableDay day2, TimetablePeriod period2, Room room2)
			throws WctttModelException, WctttAlgorithmException {

		if (semester == null) {
			throw new IllegalArgumentException("The given semester can't be null");
		}
		if (timetable == null) {
			throw new IllegalArgumentException("The given timetable can't be null");
		}
		if (day1 == null || day2 == null) {
			throw new IllegalArgumentException("The given days can't be null");
		}
		if (period1 == null || period2 == null) {
			throw new IllegalArgumentException("The given periods semester can't be null");
		}
		if (room1 == null || room2 == null) {
			throw new IllegalArgumentException("The given rooms can't be null");
		}

		List<Tuple<ConstraintType, String>> violations = new ArrayList<Tuple<ConstraintType, String>>();
		Tuple<Double, List<Tuple<ConstraintType, String>>> result = new Tuple<Double, List<Tuple<ConstraintType, String>>>(
				0.0, violations);

		TimetableAssignment assignment12 = null;// second space in case of double session
		TimetableAssignment assignment22 = null;

		ConstraintViolationsCalculator calc = new ConstraintViolationsCalculator(semester);
		// Check if the assignments are equal in case of double session.
		if (assignment1 != null && assignment2 != null) {
			if (assignment1.equals(assignment2)) {
				violations.add(new Tuple<ConstraintType, String>(null, "The assignments are the same"));
				return result;
			}
		}

		// Get the periods of the assignments
		TimetablePeriod period12 = null;// In case of double session
		TimetablePeriod period22 = null;

		List<TimetablePeriod> freePeriods1 = null;
		List<TimetablePeriod> freePeriods2 = null;

		// Remove assignments and get rooms
		if (assignment1 != null) {
			period1.removeAssignment(assignment1);
			// Get period and assignment in case of double session
			if (assignment1.getSession().isDoubleSession()) {
				period12 = getOtherPeriodForDoubleSession(timetable, assignment1, period1, day1);
				assignment12 = period12.getAssignments().stream()
						.filter(a -> a.getSession().getId().equals(assignment1.getSession().getId())).findFirst().get();
				period12.removeAssignment(assignment12);
			}
		}
		if (assignment2 != null) {
			period2.removeAssignment(assignment2);
			// Get period and assignment in case of double session
			if (assignment2.getSession().isDoubleSession()) {
				period22 = getOtherPeriodForDoubleSession(timetable, assignment2, period2, day2);
				assignment22 = period22.getAssignments().stream()
						.filter(a -> a.getSession().getId().equals(assignment2.getSession().getId())).findFirst().get();
				period22.removeAssignment(assignment22);
			}
		}
		if (assignment12 != null) {
			freePeriods2 = getFreePeriods(day2, room2, period2.getTimeSlot());
			// If none are found revert changes and return.
			if (freePeriods2.size() < 1) {
				period1.addAssignment(assignment1);
				period12.addAssignment(assignment12);
				assignment1.setRoom(room1);
				assignment12.setRoom(room1);
				if (assignment2 != null) {
					period2.addAssignment(assignment2);
					assignment2.setRoom(room2);
					if (assignment22 != null) {
						period22.addAssignment(assignment22);
						assignment22.setRoom(room2);
					}
				}
				violations.add(new Tuple<ConstraintType, String>(null, "No free time slot for double session"));
				return result;
			}
		}
		if (assignment22 != null) {
			freePeriods1 = getFreePeriods(day1, room1, period1.getTimeSlot());
			// If none are found revert changes and return.
			if (freePeriods1.size() < 1) {
				period2.addAssignment(assignment2);
				period22.addAssignment(assignment22);
				assignment2.setRoom(room2);
				assignment22.setRoom(room2);
				if (assignment1 != null) {
					period1.addAssignment(assignment1);
					assignment1.setRoom(room1);
					if (assignment12 != null) {
						period12.addAssignment(assignment12);
						assignment12.setRoom(room1);
					}
				}
				violations.add(new Tuple<ConstraintType, String>(null, "No free time slot for double session"));
				return result;
			}
		}
		// Switch rooms
		if (assignment1 != null) {
			assignment1.setRoom(room2);
		}
		if (assignment12 != null) {
			assignment12.setRoom(room2);
		}
		if (assignment2 != null) {
			assignment2.setRoom(room1);
		}
		if (assignment22 != null) {
			assignment22.setRoom(room1);
		}
		/*
		 * Check violations for assignment 1 This is done by adding assignment 2 without
		 * checking the constraints and then checking the constraints for adding
		 * assignment 1. And then remove assignment 2 again and do the same for
		 * assignment 2. The soft constraints are checked after adding assignment 2,
		 * because for this we need to add both assignments.
		 */
		if (assignment1 != null) {
			// Add assignment 2 to time slot of assignment 1
			if (assignment2 != null) {
				period1.addAssignment(assignment2);
				if (assignment22 != null) {
					freePeriods1.get(0).addAssignment(assignment22);
				}
			}
			violations.addAll(calc.calcAssignmentHardViolations(timetable, period2, assignment1));
			if (assignment12 != null) {
				violations.addAll(calc.calcAssignmentHardViolations(timetable, freePeriods2.get(0), assignment12));
			}
			// Add assignment 1 in the slot of assignment 2.
			if (assignment1 != null) {
				period2.addAssignment(assignment1);
				if (assignment12 != null) {
					freePeriods2.get(0).addAssignment(assignment12);
				}
			}
			// calculate soft constraints.
			result.x = calc.calculateViolationsPenalty(calc.calcTimetablePenalty(timetable));
			// remove assignment 1 again
			if (assignment1 != null) {
				period2.removeAssignment(assignment1);
				if (assignment12 != null) {
					freePeriods2.get(0).removeAssignment(assignment12);
				}
			}
			// remove assignment 2 again
			if (assignment2 != null) {
				period1.removeAssignment(assignment2);
				if (assignment22 != null) {
					freePeriods1.get(0).removeAssignment(assignment22);
				}
			}
		}
		// Check violations for assignment 2
		if (assignment2 != null) {
			// Add assignment 2 to time slot of assignment 1
			if (assignment1 != null) {
				period2.addAssignment(assignment1);
				if (assignment12 != null) {
					freePeriods2.get(0).addAssignment(assignment12);
				}
			}
			violations.addAll(calc.calcAssignmentHardViolations(timetable, period1, assignment2));
			if (assignment22 != null) {
				violations.addAll(calc.calcAssignmentHardViolations(timetable, freePeriods1.get(0), assignment22));
			}
			// Reverse changes
			if (assignment1 != null) {
				period2.removeAssignment(assignment1);
				if (assignment12 != null) {
					freePeriods2.get(0).removeAssignment(assignment12);
				}
			}
		}
		// Reverse rooms again and add the assignments back into their periods
		if (assignment1 != null) {
			assignment1.setRoom(room1);
			period1.addAssignment(assignment1);
			if (assignment12 != null) {
				assignment12.setRoom(room1);
				period12.addAssignment(assignment12);
			}
		}
		if (assignment2 != null) {
			assignment2.setRoom(room2);
			period2.addAssignment(assignment2);
			if (assignment22 != null) {
				assignment22.setRoom(room2);
				period22.addAssignment(assignment22);
			}
		}
		return result;
	}

	/**
	 * Checks the violations for hard constraints that would occur if an assignment
	 * would be added at a given position.
	 * 
	 * @param semester   - Semester of the timetable.
	 * @param timetable  - The {@link Timetable} containing the slots.
	 * @param assignment - The assignment in the slot that the session is to be
	 *                   moved to. Can be null in case of an empty slot.
	 * @param session    - Session to be added.
	 * @param day2       - Day of the second time slot.
	 * @param period2    - Period of the second time slot.
	 * @param room2      - Room of the second time slot.
	 * @return A tuple containing the resulting new penalty if the two were swapped
	 *         and a list containing the types of constraint violation (may be null)
	 *         and their description text.
	 * @throws WctttModelException
	 */
	public static Tuple<Double, List<Tuple<ConstraintType, String>>> getAddingViolations(Semester semester,
			Timetable timetable, TimetableAssignment assignment, Session session, TimetableDay day,
			TimetablePeriod period, Room room) throws WctttModelException {

		if (session == null) {
			throw new IllegalArgumentException("The given assignment can't be null");
		}
		if (semester == null) {
			throw new IllegalArgumentException("The given semester can't be null");
		}
		if (timetable == null) {
			throw new IllegalArgumentException("The given timetable can't be null");
		}
		if (day == null) {
			throw new IllegalArgumentException("The given day can't be null");
		}
		if (period == null) {
			throw new IllegalArgumentException("The given period semester can't be null");
		}
		if (room == null) {
			throw new IllegalArgumentException("The given rooms can't be null");
		}

		List<Tuple<ConstraintType, String>> violations = new ArrayList<Tuple<ConstraintType, String>>();
		Tuple<Double, List<Tuple<ConstraintType, String>>> result = new Tuple<Double, List<Tuple<ConstraintType, String>>>(
				0.0, violations);

		ConstraintViolationsCalculator calc = new ConstraintViolationsCalculator(semester);
		if (assignment != null) {
			period.removeAssignment(assignment);
		}
		Optional<TimetableAssignment> assignment2 = period.getAssignments().stream()
				.filter(a -> a.getRoom().equals(room)).findAny();

		if (assignment2.isPresent()) {
			violations.add(new Tuple<ConstraintType, String>(null, "The room in this time slot is already occupied"));
			return result;
		}

		TimetableAssignment newAssignment = new TimetableAssignment(session, room);
		// Get violations
		violations.addAll(calc.calcAssignmentHardViolations(timetable, period, newAssignment));
		// Get penalty
		period.addAssignment(newAssignment);
		result.x = calc.calculateViolationsPenalty(calc.calcTimetablePenalty(timetable));
		period.removeAssignment(newAssignment);
		if (assignment != null) {
			period.addAssignment(assignment);
		}
		return result;
	}

	private static TimetablePeriod getOtherPeriodForDoubleSession(Timetable timetable, TimetableAssignment assignment,
			TimetablePeriod period, TimetableDay day) throws WctttAlgorithmException {

		for (TimetablePeriod p : timetable.getDays().get(period.getDay() - 1).getPeriods()) {
			if (p.getAssignments().stream().filter(a -> a.getSession().equals(assignment.getSession())).findFirst()
					.isPresent()) {
				return p;
			}
		}
		throw new WctttAlgorithmException("Could not find second period to double session");
	}

	private static List<TimetablePeriod> getFreePeriods(TimetableDay day, Room room, int timeSlot) {
		List<TimetablePeriod> freeSlots = new ArrayList<TimetablePeriod>();
		if (timeSlot - 1 >= 0) {
			Optional<TimetablePeriod> otherPeriod = day.getPeriods().stream()
					.filter(p -> p.getTimeSlot() == timeSlot - 1).findFirst();
			if (otherPeriod.isPresent()) {
				if (otherPeriod.get().getAssignments().stream().filter(a -> a.getRoom().equals(room)).findFirst()
						.isEmpty()) {
					freeSlots.add(otherPeriod.get());
				}
			}
		}
		if (timeSlot + 1 < 7) {
			Optional<TimetablePeriod> otherPeriod = day.getPeriods().stream()
					.filter(p -> p.getTimeSlot() == timeSlot + 1).findFirst();
			if (otherPeriod.isPresent()) {
				if (otherPeriod.get().getAssignments().stream().filter(a -> a.getRoom().equals(room)).findFirst()
						.isEmpty()) {
					freeSlots.add(otherPeriod.get());
				}
			}
		}
		return freeSlots;
	}

	/**
	 * Swaps two time slots. It should be checked if there are any constraint
	 * violations before this method is called as there will be no check for hard
	 * constraint violations when swapping!
	 * 
	 * @param timetable   - The {@link Timetable} containing the slots.
	 * @param assignment1 - First assignment (first time slot). May be null if the
	 *                    slot is free.
	 * @param day1        - Day of the first time slot.
	 * @param period1     - Period of the first time slot.
	 * @param room1       - Room of the first time slot.
	 * @param assignment2 - Second assignment (second time slot). May be null if the
	 *                    slot is free.
	 * @param day2        - Day of the second time slot.
	 * @param period2     - Period of the second time slot.
	 * @param room2       - Room of the second time slot.
	 * @return True if the swap was successfull.<br>
	 *         False if there was no space for the second slot of the double
	 *         session.
	 * @throws WctttModelException
	 * @throws WctttAlgorithmException
	 */
	public static boolean swapTimeslots(Timetable timetable, TimetableAssignment assignment1, TimetableDay day1,
			TimetablePeriod period1, Room room1, TimetableAssignment assignment2, TimetableDay day2,
			TimetablePeriod period2, Room room2) throws WctttModelException, WctttAlgorithmException {

		TimetableAssignment assignment12 = null;// second space in case of double session
		TimetableAssignment assignment22 = null;

		TimetablePeriod firstPeriod2 = null;// In case of double session
		TimetablePeriod secondPeriod2 = null;
		List<TimetablePeriod> freePeriods1 = null;
		List<TimetablePeriod> freePeriods2 = null;

		// Remove assignments and get rooms
		if (assignment1 != null) {
			period1.removeAssignment(assignment1);
			// Get period and assignment in case of double session
			if (assignment1.getSession().isDoubleSession()) {
				firstPeriod2 = getOtherPeriodForDoubleSession(timetable, assignment1, period1, day1);
				assignment12 = firstPeriod2.getAssignments().stream()
						.filter(a -> a.getSession().getId().equals(assignment1.getSession().getId())).findFirst().get();
				firstPeriod2.removeAssignment(assignment12);
			}
		}
		if (assignment2 != null) {
			period2.removeAssignment(assignment2);
			// Get period and assignment in case of double session
			if (assignment2.getSession().isDoubleSession()) {
				secondPeriod2 = getOtherPeriodForDoubleSession(timetable, assignment2, period2, day2);
				assignment22 = secondPeriod2.getAssignments().stream()
						.filter(a -> a.getSession().getId().equals(assignment2.getSession().getId())).findFirst().get();
				secondPeriod2.removeAssignment(assignment22);
			}
		}
		if (assignment12 != null) {
			freePeriods2 = getFreePeriods(day2, room2, period2.getTimeSlot());
			// If none are found revert changes and return.
			if (freePeriods2.size() < 1) {
				period1.addAssignment(assignment1);
				firstPeriod2.addAssignment(assignment12);
				assignment1.setRoom(room1);
				assignment12.setRoom(room1);
				if (assignment2 != null) {
					period2.addAssignment(assignment2);
					assignment2.setRoom(room2);
					if (assignment22 != null) {
						secondPeriod2.addAssignment(assignment22);
						assignment22.setRoom(room2);
					}
				}
				return false;
			}
		}
		if (assignment22 != null) {
			freePeriods1 = getFreePeriods(day1, room1, period1.getTimeSlot());
			// If none are found revert changes and return.
			if (freePeriods1.size() < 1) {
				period2.addAssignment(assignment2);
				secondPeriod2.addAssignment(assignment22);
				assignment2.setRoom(room2);
				assignment22.setRoom(room2);
				if (assignment1 != null) {
					period1.addAssignment(assignment1);
					assignment1.setRoom(room1);
					if (assignment12 != null) {
						firstPeriod2.addAssignment(assignment12);
						assignment12.setRoom(room1);
					}
				}
				return false;
			}
		}
		// Switch rooms
		if (assignment1 != null) {
			assignment1.setRoom(room2);
		}
		if (assignment12 != null) {
			assignment12.setRoom(room2);
		}
		if (assignment2 != null) {
			assignment2.setRoom(room1);
		}
		if (assignment22 != null) {
			assignment22.setRoom(room1);
		}
		// Add the assignments to the other periods
		// Assignments are null if the slot is free!
		if (assignment2 != null) {
			period1.addAssignment(assignment2);
			if (assignment22 != null) {
				freePeriods1.get(0).addAssignment(assignment22);
			}
		}
		if (assignment1 != null) {
			period2.addAssignment(assignment1);
			if (assignment12 != null) {
				freePeriods2.get(0).addAssignment(assignment12);
			}
		}
		return true;
	}

	/**
	 * Adds a {@link Session} to the given period on a given day. If there is
	 * already a {@link Session} in the slot it will be removed and returned. If the
	 * inserted session is a double session at least one of the periods besides it
	 * have to be free.
	 * 
	 * @param session
	 * @param room
	 * @param period
	 * @param day
	 * @return The session removed from the slot in which the session was added.
	 *         Null if the slot was empty.
	 * @throws WctttModelException
	 */
	public static Session addSession(Session session, Timetable timetable, Room room, TimetablePeriod period,
			TimetableDay day) throws WctttModelException {
		Session removedSession = null;
		TimetableAssignment removedAssignment = null;
		for (TimetableAssignment assignment : period.getAssignments()) {
			if (assignment.getRoom().getName().equals(room.getName())) {
				if (assignment.getSession().isDoubleSession()) {
					throw new WctttModelException("Could not add session because a double session is in the slot.");
				} else {
					removedSession = assignment.getSession();
					removedAssignment = assignment;
				}
			}
		}
		TimetableAssignment assignment = new TimetableAssignment(session, room);
		if (removedSession != null) {
			period.removeAssignment(removedAssignment);
		}
		period.addAssignment(assignment);

		// Check if the session to be added is a double session
		if (session.isDoubleSession()) {
			if (period.getTimeSlot() + 1 > timetable.getDays().get(0).getPeriods().size()) {
				throw new WctttModelException("Double sessions can't be added in the last slot.");
			}
			// Get the second period
			TimetablePeriod secondPeriod = null;
			for (TimetablePeriod p : day.getPeriods()) {
				if (p.getTimeSlot() == period.getTimeSlot() + 1) {
					secondPeriod = p;
				}
			}
			TimetableAssignment secondRemovedAssignment = null;
			// Get the session from the second slot
			for (TimetableAssignment assign : secondPeriod.getAssignments()) {
				if (assign.getRoom().getName().equals(room.getName())) {
					if (assign.getSession().isDoubleSession()) {
						throw new WctttModelException("Could not add session because a double session is in the slot.");
					} else {
						if (removedSession != null) {
							throw new WctttModelException(
									"Could not add session because a two sessions is in the needed slots.");
						} else {
							removedSession = assign.getSession();
							secondRemovedAssignment = assign;
						}
					}
				}
			}
			if (secondRemovedAssignment != null) {
				secondPeriod.removeAssignment(secondRemovedAssignment);
			}
			secondPeriod.addAssignment(assignment);
		}

		return removedSession;
	}
}
