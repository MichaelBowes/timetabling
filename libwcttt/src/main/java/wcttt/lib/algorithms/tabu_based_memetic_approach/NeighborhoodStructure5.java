/*
 * WCT³ (WIAI Course Timetabling Tool) is a software that strives to automate
 * the timetabling process at the WIAI faculty of the University of Bamberg.
 *
 * libwcttt comprises the data model, a binder (parser + emitter) to store the
 * data as XML files, the implementations of the algorithms as well as
 * functionality to calculate conflicts and their violations.
 *
 * Copyright (C) 2018 Nicolas Gross
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package wcttt.lib.algorithms.tabu_based_memetic_approach;

import wcttt.lib.algorithms.WctttAlgorithmException;
import wcttt.lib.algorithms.WctttAlgorithmFatalException;
import wcttt.lib.model.*;
import wcttt.lib.util.TimetableUtil;

import java.util.Collections;
import java.util.List;

public class NeighborhoodStructure5 extends NeighborhoodStructure4 {	

	private static final String NAME = "Nbs5";
	private static final String DESCRIPTION = "Chooses a sample of 20% of assignments at "
			+ "random and moves the two assignments with the highest penalty out of them "
			+ "to a new random feasible timeslot.";
	
	public NeighborhoodStructure5(Semester semester) {
		super(semester);
	}
	/**
	 * Chooses a sample of 20% of assignments at random and moves the two
	 * assignments with the highest penalty out of them to a new random feasible
	 * timeslot.
	 *
	 * @param timetable the timetable to which the neighborhood structure should be
	 *                  applied.
	 * @param semester  the semester the timetable belongs to.
	 */
	@Override
	protected void applyImpl(Timetable timetable, Semester semester) throws WctttAlgorithmException {
		List<TimetableAssignment> twentyPercent = selectAssignmentPercentage(timetable, semester, 0.2);
		List<TimetableAssignment> highestPenaltyAssignments = findHighestPenaltyAssignments(twentyPercent, timetable, semester);
		
		for (TimetableAssignment randomAssgmt : highestPenaltyAssignments) {

			if(!TimetableUtil.removeSessionAssignments(timetable, randomAssgmt)) {
				throw new WctttAlgorithmFatalException("Could not remove " + randomAssgmt + " from the timetable");
			}
			
			// Because external sessions must have a pre-assignment:
			assert randomAssgmt.getSession() instanceof InternalSession;
			assert randomAssgmt.getRoom() instanceof InternalRoom;

			List<Period> periods = Util.createPeriodList(semester);
			Collections.shuffle(periods);

			if (!Util.assignSessionRandomly((InternalSession) randomAssgmt.getSession(), timetable, semester, periods,
					null, null, null, null)) {
				throw new WctttAlgorithmFatalException(
						"Implementation error, " + "could not find suitable room and period for new "
								+ "assignment, at least the period and room of the previous"
								+ " assignment should have been found");
			}
		}
	}

	@Override
	public String getDescription() {
		return DESCRIPTION;
	}
	
	@Override
	public String getName() {
		return NAME;
	}
}
