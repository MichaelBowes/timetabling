package wcttt.gui.model;

import wcttt.lib.model.Session;

public class SelectedSession implements Selection {

	private Session session;
	
	public SelectedSession(Session session) {
		this.session = session;
	}
	
	@Override
	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}	
}
