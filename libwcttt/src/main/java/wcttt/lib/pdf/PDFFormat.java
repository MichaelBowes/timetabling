package wcttt.lib.pdf;

/**
 * @author Nicolas Bruch
 *
 */
public enum PDFFormat {
	A0(841, 1189),
	A1(594, 841),
	A2(420, 594),
	A3(297, 420),
	A4(210, 297),
	A5(148, 210),
	A6(105, 148);
	
	private static final float POINTS_PER_INCH = 72;
	private static final float POINTS_PER_MM = 1 / (10 * 2.54f) * POINTS_PER_INCH;
	private float width;
	private float height;
	private boolean isHorizontal;
	
	PDFFormat(float width, float height){
		this.width = width * POINTS_PER_MM;
		this.height = height * POINTS_PER_MM;
	}
	
	public void setHorizontal(boolean horizontal) {
		this.isHorizontal = horizontal;
	}
	
	public boolean isHorizontal() {
		return this.isHorizontal;
	}
	
	public float getWidth() {
		if(isHorizontal) {
			return height;
		}
		return this.width;
	}
	
	public float getHeight() {
		if(isHorizontal) {
			return width;
		}
		return this.height;
	}
	
	/**
	 * @return Width value in millimeter.
	 */
	public float getMMWidth() {
		return getWidth() / POINTS_PER_MM;
	}
	
	/**
	 * @return Height value in millimeter.
	 */
	public float getMMHeight() {
		return getHeight() / POINTS_PER_MM;
	}
	
}
