package wcttt.gui.model;

import wcttt.lib.model.Session;

/**
 * @author Nicolas Bruch
 * 
 *         Represents an selected object in the gui.
 *
 */
public interface Selection {

	Session getSession();
}
