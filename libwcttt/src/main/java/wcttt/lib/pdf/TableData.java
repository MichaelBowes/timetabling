package wcttt.lib.pdf;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nicolas Bruch
 *
 */
public class TableData {

	private String pageHeader;
	private String tableHeader;
	private List<List<String>> cellData = new ArrayList<List<String>>();
	
	public TableData(String pageHeader, String tableHeader) {
		this.pageHeader = pageHeader;
		this.tableHeader = tableHeader;
	}
	
	/**
	 * Returns a specific cell of the table.
	 * If the column or row are out of bounds null is returned.
	 * @param row of the table.
	 * @param column of the table.
	 * @return
	 * Value of cell or null if it does not exist.
	 */
	public String getCellData(int row, int column) {
		String result = null;
		try {
			List<String> selectedRow = cellData.get(row);
			if(selectedRow != null) {
				result = selectedRow.get(column);
			}
		}catch(IndexOutOfBoundsException e) {
			return null;
		}
		return result;
	}
	
	/**
	 * Returns a specific row of the table.
	 * If the index is out of bounds null will be returned.
	 * @param index
	 * @return
	 */
	public List<String> getRow(int index){
		List<String> row = null;
		try {
			row = cellData.get(index);
		}catch(IndexOutOfBoundsException e) {
			return null;
		}
		return row;
	}
	
	/**
	 * Returns a specific row of the table.
	 * If the index is out of bounds null will be returned.
	 * @param index
	 * @return
	 */
	public List<String> getColumn(int index){
		List<String> column = new ArrayList<String>();
		try {
			for(List<String> row : cellData) {
				column.add(row.get(index));
			}
		}catch(IndexOutOfBoundsException e) {
			return null;
		}
		return column;
	}
	
	public String getTableHeader() {
		return tableHeader;
	}

	public void setTableHeader(String tableHeader) {
		this.tableHeader = tableHeader;
	}

	public void setCellData(List<List<String>> cellData) {
		this.cellData = cellData;
	}
	
	public List<List<String>> getCellData(){
		return this.cellData;
	}

	public void addRow(List<String> row) {
		cellData.add(row);
	}
	
	public void addRows(List<List<String>> rows) {
		cellData.addAll(rows);
	}

	public String getPageHeader() {
		return pageHeader;
	}

	public void setPageHeader(String pageHeader) {
		this.pageHeader = pageHeader;
	}	
}
