package wcttt.lib.algorithms.tabu_based_memetic_approach;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import wcttt.lib.algorithms.WctttAlgorithmException;
import wcttt.lib.binder.WctttBinder;
import wcttt.lib.binder.WctttBinderException;
import wcttt.lib.model.Semester;
import wcttt.lib.model.Timetable;
import wcttt.lib.util.ConstraintViolationsCalculator;

public class SaturationDegreeHeuristicBenchmark {

	/**
	 * Arguments are
	 * <ul>
	 * <li>File path</li>
	 * <li>Starting number of population</li>
	 * <li>Ending number of population</li>
	 * <li>Increment of population number</li>
	 * <li>Number of generations per population number</li>
	 * </ul>
	 * @param args
	 */
	public static void main(String[] args) {
		if(args.length != 5) {
			throw new IllegalArgumentException("Not enough arguments."
					+ " (path, start, end, increment, tries).");
		}
		File file = new File(args[0]);
		if(!file.exists()) {
			throw new IllegalArgumentException("Could not find file.");
		}
		int lower;
		try {
			lower = Integer.parseInt(args[1]);
		}catch(NumberFormatException e) {
			throw new IllegalArgumentException("Invalid argument '" + args[1] + "' is not a valid number.");
		}
		int upper;
		try {
			upper = Integer.parseInt(args[2]);
		}catch(NumberFormatException e) {
			throw new IllegalArgumentException("Invalid argument '" + args[2] + "' is not a valid number.");
		}
		int increment;
		try {
			increment = Integer.parseInt(args[3]);
		}catch(NumberFormatException e) {
			throw new IllegalArgumentException("Invalid argument '" + args[3] + "' is not a valid number.");
		}
		int count;
		try {
			count = Integer.parseInt(args[4]);
		}catch(NumberFormatException e) {
			throw new IllegalArgumentException("Invalid argument '" + args[4] + "' is not a valid number.");
		}
		Semester semester = null;
		try {
			WctttBinder binder = new WctttBinder(file);
			semester = binder.parse();
		} catch (WctttBinderException e) {
			throw new RuntimeException("Could not parse the given file. " + e.getMessage());
		}
		ConstraintViolationsCalculator constrCalc =
				new ConstraintViolationsCalculator(semester);
		AtomicBoolean cancelled = new AtomicBoolean(false);
		SaturationDegreeHeuristic sdh =  new SaturationDegreeHeuristic(semester);
		Map<Integer,List<Double>> results = new HashMap<Integer, List<Double>>();
		for(int i = lower; i <= upper; i += increment) {
			for(int j = 0; j < count; j++) {
				try {
					double lowestPenalty = getLowestPenalty(sdh.generateFeasibleSolutions(i, cancelled), constrCalc);
					if(results.containsKey(i)) {
						results.get(i).add(lowestPenalty);
					}else {
						List<Double> list = new LinkedList<Double>();
						list.add(lowestPenalty);
						results.put(i, list);
					}					
				} catch (WctttAlgorithmException e) {
					throw new RuntimeException("Failed to generate feasible solution. " + e.getMessage());
				}
			}
		}
		printResults(results);
	}
	
	private static double getLowestPenalty(List<Timetable> timetables, ConstraintViolationsCalculator constrCalc) {
		if(timetables == null || timetables.isEmpty()) {
			throw new IllegalArgumentException("Can't select penalty from list that is empty or null.");
		}		
		timetables.forEach(t -> t.setSoftConstraintPenalty(constrCalc.calculateViolationsPenalty(
				constrCalc.calcTimetablePenalty(t))));
		Timetable lowest = null;
		for(Timetable tt : timetables) {
			if(lowest == null) {
				lowest = tt;
			}else {
				if(lowest.getSoftConstraintPenalty() > tt.getSoftConstraintPenalty()) {
					lowest = tt;
				}
			}
		}
		return lowest.getSoftConstraintPenalty();
	}
	
	private static void printResults(Map<Integer, List<Double>> results) {
		for(Map.Entry<Integer, List<Double>> entry : results.entrySet()) {
			System.out.println("Population size: " + entry.getKey());
			for(Double d : entry.getValue()) {
				System.out.print(Util.round(d, 2) + ", ");
			}
			System.out.println();
			System.out.println();
		}
	}
	
}
