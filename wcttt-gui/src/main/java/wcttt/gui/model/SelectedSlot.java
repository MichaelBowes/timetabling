package wcttt.gui.model;

import wcttt.lib.model.Session;
import wcttt.lib.model.TimetableAssignment;

public class SelectedSlot implements Selection {

	private TimetableAssignment assignment;
	private String day;
	private int period;
	private String room;
	
	public SelectedSlot(TimetableAssignment assignment, String day, int period, String room) {
		this.assignment = assignment;
		this.day = day;
		this.period = period;
		this.room = room;
	}
	
	public TimetableAssignment getAssignment() {
		return assignment;
	}
	public void setAssignment(TimetableAssignment assignment) {
		this.assignment = assignment;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public int getPeriod() {
		return period;
	}
	public void setPeriod(int period) {
		this.period = period;
	}
	public String getRoom() {
		return room;
	}
	public void setRoom(String room) {
		this.room = room;
	}

	@Override
	public Session getSession() {
		if(assignment == null) {
			return null;
		}
		return assignment.getSession();
	}
	
}
