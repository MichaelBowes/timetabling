/*
 * WCT³ (WIAI Course Timetabling Tool) is a software that strives to automate
 * the timetabling process at the WIAI faculty of the University of Bamberg.
 *
 * WCT³-GUI comprises functionality to view generated timetables, edit semester
 * data and to generate new timetables.
 *
 * Copyright (C) 2018 Nicolas Gross
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package wcttt.gui.controller;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Map;

import javafx.application.HostServices;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import wcttt.gui.model.Model;
import wcttt.lib.model.ConstraintType;
import wcttt.lib.model.Timetable;
import wcttt.lib.util.ConstraintViolationsCalculator;
import wcttt.lib.util.ListWrapper;

/**
 * Controller for the tab that shows the statistics for both, neighbourhood structures and penalties
 * from soft constraint violations.
 * 
 * @author Michael Bowes
 * @author David Glombik
 */
public class MainStatisticsController extends Controller {

	@FXML
	private VBox barChartBox;
	private Button showChartButton;
	private Button showPenaltyButton;
	private BarChart<String, Double> neighbourhoodStructureBarChart;
	private BarChart<String, Double> softConstraintBarChart;
	private boolean tableSelected;

	private Label averagePenaltyLabel;
	private Label softConstraintPenaltyLabel;

	@FXML
	protected void initialize() {

		barChartBox.setSpacing(10);
		barChartBox.setPadding(new Insets(10, 3, 10, 3));
		showChartButton = new Button("show Neighborhood Structure uses");
		showChartButton.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		showChartButton.setOnAction(event -> {
			if (tableSelected) {
				clearStatisticsTab();
				initBarChartNeighbourhoodStructures();
				initPenaltyList();
			}
		});
		showPenaltyButton = new Button("show Penalties");
		showPenaltyButton.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		showPenaltyButton.setOnAction(event -> {
			if (tableSelected) {
				clearStatisticsTab();
				initBarChartSoftConstraint();
				initPenaltyListSoftConstraints();
			}
		});
		barChartBox.getChildren().add(showChartButton);
		barChartBox.getChildren().add(showPenaltyButton);
	}

	/**
	 * Creates label for the total average penalty change of the selected
	 * neighborhood structure.
	 */
	private String createLabel(int i, Map<String, ListWrapper<Double>> neighbourhoodStructurePenalty, Timetable timetable) {
		String neighbourhoodStructureString;
		double sum = 0;
		ListWrapper<Double> values = neighbourhoodStructurePenalty.get("Nbs" + i);
		if(values == null) {
			return " - ";
		}			
		for (Double d : neighbourhoodStructurePenalty.get("Nbs" + i).getList()) {
			sum += d;
		}
		double avg = sum / timetable.getNeighbourhoodStructureCount().get("Nbs" + i);

		BigDecimal bigDec = new BigDecimal(avg);
		MathContext mc = new MathContext(3);
		BigDecimal bigDecRounded = bigDec.round(mc);
		neighbourhoodStructureString = "Nbs" + i + ": " + bigDecRounded;
		return neighbourhoodStructureString;
	}

	/**
	 * Initializes the PenaltyList with the average delta penalty data of the selected
	 * timetable.
	 */
	private void initPenaltyList() throws NullPointerException {
		averagePenaltyLabel = new Label("Average Penalty Change:");
		averagePenaltyLabel.setUnderline(true);
		barChartBox.getChildren().add(averagePenaltyLabel);
		Timetable timetable = getMainController().getTimetableTableController().getSelectedTimetable();
		Map<String, ListWrapper<Double>> neighbourhoodStructurePenalty = timetable.getPenaltyDelta();

		if (!timetable.getNeighbourhoodStructureCount().isEmpty()) {
			if (timetable.getNeighbourhoodStructureCount().get("Nbs1") > 0) {
				Label lb1 = new Label(createLabel(2, neighbourhoodStructurePenalty, timetable));
				barChartBox.getChildren().add(lb1);
			}
			if (timetable.getNeighbourhoodStructureCount().get("Nbs2") > 0) {
				Label lb2 = new Label(createLabel(3, neighbourhoodStructurePenalty, timetable));
				barChartBox.getChildren().add(lb2);
			}
			if (timetable.getNeighbourhoodStructureCount().get("Nbs3") > 0) {
				Label lb3 = new Label(createLabel(4, neighbourhoodStructurePenalty, timetable));
				barChartBox.getChildren().add(lb3);
			}
			if (timetable.getNeighbourhoodStructureCount().get("Nbs4") > 0) {
				Label lb4 = new Label(createLabel(5, neighbourhoodStructurePenalty, timetable));
				barChartBox.getChildren().add(lb4);
			}
			if (timetable.getNeighbourhoodStructureCount().get("Nbs5") > 0) {
				Label lb5 = new Label(createLabel(6, neighbourhoodStructurePenalty, timetable));
				barChartBox.getChildren().add(lb5);
			}
		}
	}

	/**
	 * Initializes the BarChart with the neighborhood structure data of the selected
	 * timetable.
	 */
	private void initBarChartNeighbourhoodStructures() {
		CategoryAxis xAxis = new CategoryAxis();
		xAxis.setLabel("Neighbourhood Structures");

		NumberAxis yAxis = new NumberAxis();
		yAxis.setLabel("Use Count");

		neighbourhoodStructureBarChart = new BarChart(xAxis, yAxis);

		XYChart.Series dataSeries = fillChartNeighbourhoodStructure();

		neighbourhoodStructureBarChart.getData().add(dataSeries);
		barChartBox.getChildren().add(neighbourhoodStructureBarChart);
	}

	/**
	 * Fills the BarChart with the count of appliances of the individual
	 * neighborhood structures that were used for generating that timetable.
	 */
	private XYChart.Series<String, Double> fillChartNeighbourhoodStructure() {
		Timetable timetable = getMainController().getTimetableTableController().getSelectedTimetable();
		Map<String, Integer> neighbourhoodStructureCounts = timetable.getNeighbourhoodStructureCount();

		XYChart.Series<String, Double> dataSeries = new XYChart.Series<String, Double>();
		dataSeries.setName(timetable.getName());

		if (neighbourhoodStructureCounts.isEmpty()) {
			dataSeries.getData().add(new XYChart.Data("Nbs1", 0));
			dataSeries.getData().add(new XYChart.Data("Nbs2", 0));
			dataSeries.getData().add(new XYChart.Data("Nbs3", 0));
			dataSeries.getData().add(new XYChart.Data("Nbs4", 0));
			dataSeries.getData().add(new XYChart.Data("Nbs5", 0));
		} else {
			if (neighbourhoodStructureCounts.get("Nbs1") > 0) {
				dataSeries.getData().add(new XYChart.Data("Nbs1", neighbourhoodStructureCounts.get("Nbs1")));
			} else {
				dataSeries.getData().add(new XYChart.Data("Nbs1", 0));
			}
			if (neighbourhoodStructureCounts.get("Nbs2") > 0) {
				dataSeries.getData().add(new XYChart.Data("Nbs2", neighbourhoodStructureCounts.get("Nbs2")));
			} else {
				dataSeries.getData().add(new XYChart.Data("Nbs2", 0));
			}
			if (neighbourhoodStructureCounts.get("Nbs3") > 0) {
				dataSeries.getData().add(new XYChart.Data("Nbs3", neighbourhoodStructureCounts.get("Nbs3")));
			} else {
				dataSeries.getData().add(new XYChart.Data("Nbs3", 0));
			}
			if (neighbourhoodStructureCounts.get("Nbs4") > 0) {
				dataSeries.getData().add(new XYChart.Data("Nbs4", neighbourhoodStructureCounts.get("Nbs4")));
			} else {
				dataSeries.getData().add(new XYChart.Data("Nbs4", 0));
			}
			if (neighbourhoodStructureCounts.get("Nbs5") > 0) {
				dataSeries.getData().add(new XYChart.Data("Nbs5", neighbourhoodStructureCounts.get("Nbs5")));
			} else {
				dataSeries.getData().add(new XYChart.Data("Nbs5", 0));
			}
		}

		return dataSeries;

	}

	/**
	 * Initializes the PenaltyList for the soft constraints, with the penalty data of the selected
	 * timetable.
	 */
	private void initPenaltyListSoftConstraints() {

		Timetable timetable = getMainController().getTimetableTableController().getSelectedTimetable();
		ConstraintViolationsCalculator constraintViolationsCalculator = new ConstraintViolationsCalculator(getModel().getSemester());
		Map<ConstraintType, Integer> penaltyMap = constraintViolationsCalculator.calcTimetablePenalty(timetable);
		Double softConstraintPenalty = timetable.getSoftConstraintPenalty();

		softConstraintPenaltyLabel = new Label("Penalty: " + Util.round(softConstraintPenalty, 3));
		double width = (barChartBox.getPrefWidth() / 3);
		softConstraintPenaltyLabel.setPrefWidth(width);
		softConstraintPenaltyLabel.setStyle("-fx-font-size: 15;" + "-fx-font-weight: bold");
		softConstraintPenaltyLabel.setAlignment(Pos.CENTER);
		softConstraintPenaltyLabel.setUnderline(true);

		Label s1 = new Label("S1:  " + penaltyMap.get(ConstraintType.s1));
		s1.setPrefWidth(width);
		Label s2 = new Label("S2:  " + penaltyMap.get(ConstraintType.s2));
		s2.setPrefWidth(width);
		Label s3 = new Label("S3:  " + penaltyMap.get(ConstraintType.s3));
		s3.setPrefWidth(width);
		Label s4 = new Label("S4:  " + penaltyMap.get(ConstraintType.s4));
		s4.setPrefWidth(width);
		Label s5 = new Label("S5:  " + penaltyMap.get(ConstraintType.s5));
		s5.setPrefWidth(width);
		Label s6 = new Label("S6:  " + penaltyMap.get(ConstraintType.s6));
		s6.setPrefWidth(width);
		Label s7 = new Label("S7:  " + penaltyMap.get(ConstraintType.s7));
		s7.setPrefWidth(width);
		GridPane comparisonGrid = new GridPane();
		comparisonGrid.setGridLinesVisible(false);
		comparisonGrid.setVgap(5);
		comparisonGrid.setHgap(5);

		Label compSoftConstraintPenalty = new Label();
		compSoftConstraintPenalty.setStyle("-fx-font-size: 15;" + "-fx-font-weight: bold");
		compSoftConstraintPenalty.setAlignment(Pos.CENTER);
		compSoftConstraintPenalty.setUnderline(true);
		Label constraint1 = new Label();
		constraint1.setPrefWidth(width);
		Label constraint2 = new Label();
		constraint2.setPrefWidth(width);
		Label constraint3 = new Label();
		constraint3.setPrefWidth(width);
		Label constraint4 = new Label();
		constraint4.setPrefWidth(width);
		Label constraint5 = new Label();
		constraint5.setPrefWidth(width);
		Label constraint6 = new Label();
		constraint6.setPrefWidth(width);
		Label constraint7 = new Label();
		constraint7.setPrefWidth(width);

		ComboBox<Timetable> choices = new ComboBox<Timetable>();

		choices.getItems().addAll(getModel().getTimetables());
		choices.setPrefWidth(width);
		choices.setPromptText("Choose timetable for comparison");
		Button startComparison = new Button("Start comparison");
		startComparison.setMaxWidth(Double.MAX_VALUE);
		startComparison.setAlignment(Pos.CENTER);
		startComparison.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				Timetable comparisonTimetable = choices.getSelectionModel().getSelectedItem();
				if (comparisonTimetable != null) {
					Double comparisonSoftConstraintPenalty = comparisonTimetable.getSoftConstraintPenalty();
					Map<ConstraintType, Integer> comparisonPenaltyMap = constraintViolationsCalculator.calcTimetablePenalty(comparisonTimetable);
					compSoftConstraintPenalty.setText("Penalty: " + Util.round(comparisonSoftConstraintPenalty, 3));
					constraint1.setText("S1:   " + comparisonPenaltyMap.get(ConstraintType.s1));
					constraint2.setText("S2:   " + comparisonPenaltyMap.get(ConstraintType.s2));
					constraint3.setText("S3:   " + comparisonPenaltyMap.get(ConstraintType.s3));
					constraint4.setText("S4:   " + comparisonPenaltyMap.get(ConstraintType.s4));
					constraint5.setText("S5:   " + comparisonPenaltyMap.get(ConstraintType.s5));
					constraint6.setText("S6:   " + comparisonPenaltyMap.get(ConstraintType.s6));
					constraint7.setText("S7:   " + comparisonPenaltyMap.get(ConstraintType.s7));
				}

			}
		});

		barChartBox.getChildren().add(comparisonGrid);
		comparisonGrid.add(softConstraintPenaltyLabel, 0, 0);
		comparisonGrid.add(s1, 0, 1);
		comparisonGrid.add(s2, 0, 2);
		comparisonGrid.add(s3, 0, 3);
		comparisonGrid.add(s4, 0, 4);
		comparisonGrid.add(s5, 0, 5);
		comparisonGrid.add(s6, 0, 6);
		comparisonGrid.add(s7, 0, 7);
		comparisonGrid.add(choices, 1, 0);
		comparisonGrid.add(startComparison, 1, 2);
		comparisonGrid.add(compSoftConstraintPenalty, 2, 0);
		comparisonGrid.add(constraint1, 2, 1);
		comparisonGrid.add(constraint2, 2, 2);
		comparisonGrid.add(constraint3, 2, 3);
		comparisonGrid.add(constraint4, 2, 4);
		comparisonGrid.add(constraint5, 2, 5);
		comparisonGrid.add(constraint6, 2, 6);
		comparisonGrid.add(constraint7, 2, 7);
	}

	/**
	 * Initializes the BarChart with the soft constraint penalties data of the
	 * selected timetable.
	 */
	private void initBarChartSoftConstraint() {
		CategoryAxis xAxis = new CategoryAxis();
		xAxis.setLabel("Soft Constraints");

		NumberAxis yAxis = new NumberAxis();
		yAxis.setLabel("Violation Count");

		softConstraintBarChart = new BarChart(xAxis, yAxis);

		softConstraintBarChart.getData().addAll(fillChartSoftConstraint());
		barChartBox.getChildren().add(softConstraintBarChart);
	}

	/**
	 * Fills the BarChart with the count of soft constraint violations that occurred
	 * while generating that timetable.
	 */
	private ObservableList<XYChart.Series<String, Double>> fillChartSoftConstraint() {
		Timetable timetable = getMainController().getTimetableTableController().getSelectedTimetable();
		ConstraintViolationsCalculator calc = new ConstraintViolationsCalculator(getModel().getSemester());
		Map<ConstraintType, Integer> penaltyMap = calc.calcTimetablePenalty(timetable);

		ObservableList<Timetable> listOfTimetables = getModel().getTimetables();

		double[] averagePenalties = new double[7];
		int averagePenaltiesCell = 0;
		for (ConstraintType t : ConstraintType.values()) {
			if (t.toString().contains("s")) {
				double averageValue = 0;
				for (int i = 0; i < listOfTimetables.size(); i++) {
					averageValue = averageValue
							+ calc.calcTimetablePenalty(listOfTimetables.get(i)).get(t);
				}
				averageValue = averageValue / listOfTimetables.size();
				averagePenalties[averagePenaltiesCell] = averageValue;
				averagePenaltiesCell++;
			}
		}

		XYChart.Series<String, Double> dataSeries = new XYChart.Series<String, Double>();
		XYChart.Series<String, Double> averageSeries = new XYChart.Series<String, Double>();
		dataSeries.setName(timetable.getName());
		averageSeries.setName("Average");

		if (penaltyMap.isEmpty()) {
			dataSeries.getData().add(new XYChart.Data("SC1", 0));
			dataSeries.getData().add(new XYChart.Data("SC2", 0));
			dataSeries.getData().add(new XYChart.Data("SC3", 0));
			dataSeries.getData().add(new XYChart.Data("SC4", 0));
			dataSeries.getData().add(new XYChart.Data("SC5", 0));
			dataSeries.getData().add(new XYChart.Data("SC6", 0));
			dataSeries.getData().add(new XYChart.Data("SC7", 0));
			averageSeries.getData().add(new XYChart.Data("SC1",0));
			averageSeries.getData().add(new XYChart.Data("SC2",0));
			averageSeries.getData().add(new XYChart.Data("SC3",0));
			averageSeries.getData().add(new XYChart.Data("SC4",0));
			averageSeries.getData().add(new XYChart.Data("SC5",0));
			averageSeries.getData().add(new XYChart.Data("SC6",0));
			averageSeries.getData().add(new XYChart.Data("SC7",0));
		} else {
			if (penaltyMap.get(ConstraintType.s1) > 0) {
				dataSeries.getData().add(new XYChart.Data("SC1", penaltyMap.get(ConstraintType.s1)));
				averageSeries.getData().add(new XYChart.Data("SC1", averagePenalties[0]));
			} else {
				dataSeries.getData().add(new XYChart.Data("SC1", 0));
				averageSeries.getData().add(new XYChart.Data("SC1", averagePenalties[0]));
			}
			if (penaltyMap.get(ConstraintType.s2) > 0) {
				dataSeries.getData().add(new XYChart.Data("SC2", penaltyMap.get(ConstraintType.s2)));
				averageSeries.getData().add(new XYChart.Data("SC2", averagePenalties[1]));
			} else {
				dataSeries.getData().add(new XYChart.Data("SC2", 0));
				averageSeries.getData().add(new XYChart.Data("SC2", averagePenalties[1]));
			}
			if (penaltyMap.get(ConstraintType.s3) > 0) {
				dataSeries.getData().add(new XYChart.Data("SC3", penaltyMap.get(ConstraintType.s3)));
				averageSeries.getData().add(new XYChart.Data("SC3", averagePenalties[2]));
			} else {
				dataSeries.getData().add(new XYChart.Data("SC3", 0));
				averageSeries.getData().add(new XYChart.Data("SC3", averagePenalties[2]));
			}
			if (penaltyMap.get(ConstraintType.s4) > 0) {
				dataSeries.getData().add(new XYChart.Data("SC4", penaltyMap.get(ConstraintType.s4)));
				averageSeries.getData().add(new XYChart.Data("SC4", averagePenalties[3]));
			} else {
				dataSeries.getData().add(new XYChart.Data("SC4", 0));
				averageSeries.getData().add(new XYChart.Data("SC4", averagePenalties[3]));
			}
			if (penaltyMap.get(ConstraintType.s5) > 0) {
				dataSeries.getData().add(new XYChart.Data("SC5", penaltyMap.get(ConstraintType.s5)));
				averageSeries.getData().add(new XYChart.Data("SC5", averagePenalties[4]));
			} else {
				dataSeries.getData().add(new XYChart.Data("SC5", 0));
				averageSeries.getData().add(new XYChart.Data("SC5", averagePenalties[4]));
			}
			if (penaltyMap.get(ConstraintType.s6) > 0) {
				dataSeries.getData().add(new XYChart.Data("SC6", penaltyMap.get(ConstraintType.s6)));
				averageSeries.getData().add(new XYChart.Data("SC6", averagePenalties[5]));
			} else {
				dataSeries.getData().add(new XYChart.Data("SC6", 0));
				averageSeries.getData().add(new XYChart.Data("SC6", averagePenalties[5]));
			}
			if (penaltyMap.get(ConstraintType.s7) > 0) {
				dataSeries.getData().add(new XYChart.Data("SC7", penaltyMap.get(ConstraintType.s7)));
				averageSeries.getData().add(new XYChart.Data("SC7", averagePenalties[6]));
			} else {
				dataSeries.getData().add(new XYChart.Data("SC7", 0));
				averageSeries.getData().add(new XYChart.Data("SC7", averagePenalties[6]));
			}
		}

		ObservableList<XYChart.Series<String, Double>> data = FXCollections.observableArrayList();
		data.addAll(dataSeries, averageSeries);
		return data;

	}

	/**
	 * Clears the elements of the statistics tab and re-adds the default button elements.
	 * Should be called whenever the statistics tab needs to be reset to its default state.
	 */
	public void clearStatisticsTab() {
		barChartBox.getChildren().clear();

		showChartButton = new Button("show Neighbourhood Structure usage");
		showChartButton.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		showChartButton.setOnAction(event -> {
			if (tableSelected) {
				clearStatisticsTab();
				initBarChartNeighbourhoodStructures();
				initPenaltyList();
			}
		});
		showPenaltyButton = new Button("show Soft Constraint penalties");
		showPenaltyButton.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		showPenaltyButton.setOnAction(event -> {
			if (tableSelected) {
				clearStatisticsTab();
				initBarChartSoftConstraint();
				initPenaltyListSoftConstraints();
			}
		});
		barChartBox.getChildren().add(showChartButton);
		barChartBox.getChildren().add(showPenaltyButton);
	}

	/**
	 * Updates the tableSelected boolean if a timetable is selected or deselected.
	 * 
	 * @param tableSelected
	 */
	public void timetableSelected(boolean tableSelected) {
		this.tableSelected = tableSelected;
	}

	@Override
	public void setup(Stage stage, HostServices hostServices, MainController mainController, Model model) {
		super.setup(stage, hostServices, mainController, model);
	}

}
