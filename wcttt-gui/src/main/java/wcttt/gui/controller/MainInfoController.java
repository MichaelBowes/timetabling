package wcttt.gui.controller;

import javafx.application.HostServices;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.TitledPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;
import wcttt.gui.model.Model;
import wcttt.lib.model.ConstraintType;

public class MainInfoController extends Controller {

	public MainInfoController() {

	}

	@FXML
	private TextFlow hc1Text;
	@FXML
	private TextFlow hc2Text;
	@FXML
	private TextFlow hc3Text;
	@FXML
	private TextFlow hc4Text;
	@FXML
	private TextFlow hc5Text;
	@FXML
	private TextFlow hc6Text;
	@FXML
	private TextFlow hc7Text;
	@FXML
	private TextFlow hc8Text;
	@FXML
	private TextFlow hc9Text;
	@FXML
	private TextFlow hc10Text;

	@FXML
	private TextFlow sc1Text;
	@FXML
	private TextFlow sc2Text;
	@FXML
	private TextFlow sc3Text;
	@FXML
	private TextFlow sc4Text;
	@FXML
	private TextFlow sc5Text;
	@FXML
	private TextFlow sc6Text;
	@FXML
	private TextFlow sc7Text;

	@FXML
	private TitledPane sc1Title;
	@FXML
	private TitledPane sc2Title;
	@FXML
	private TitledPane sc3Title;
	@FXML
	private TitledPane sc4Title;
	@FXML
	private TitledPane sc5Title;
	@FXML
	private TitledPane sc6Title;
	@FXML
	private TitledPane sc7Title;

	@FXML
	private TextFlow nbs1Text;
	@FXML
	private TextFlow nbs2Text;
	@FXML
	private TextFlow nbs3Text;
	@FXML
	private TextFlow nbs4Text;
	@FXML
	private TextFlow nbs5Text;

	@FXML
	public void initialize() {
	}

	@Override
	public void setup(Stage stage, HostServices hostServices, MainController mainController, Model model) {
		super.setup(stage, hostServices, mainController, model);
		Platform.runLater(this::initGui);
	}

	public void updateGui() {
		sc1Title.setText("Soft Constraint 1" + " (" + String.valueOf(getModel().getConstrWeightings().getS1()) + ") ");
		sc2Title.setText("Soft Constraint 2" + " (" + String.valueOf(getModel().getConstrWeightings().getS2()) + ") ");
		sc3Title.setText("Soft Constraint 3" + " (" + String.valueOf(getModel().getConstrWeightings().getS3()) + ") ");
		sc4Title.setText("Soft Constraint 4" + " (" + String.valueOf(getModel().getConstrWeightings().getS4()) + ") ");
		sc5Title.setText("Soft Constraint 5" + " (" + String.valueOf(getModel().getConstrWeightings().getS5()) + ") ");
		sc6Title.setText("Soft Constraint 6" + " (" + String.valueOf(getModel().getConstrWeightings().getS6()) + ") ");
		sc7Title.setText("Soft Constraint 7" + " (" + String.valueOf(getModel().getConstrWeightings().getS7()) + ") ");
	}

	public void initGui() {
		hc1Text.getChildren().add(new Text(ConstraintType.h1.getDescription()));
		hc2Text.getChildren().add(new Text(ConstraintType.h2.getDescription()));
		hc3Text.getChildren().add(new Text(ConstraintType.h3.getDescription()));
		hc4Text.getChildren().add(new Text(ConstraintType.h4.getDescription()));
		hc5Text.getChildren().add(new Text(ConstraintType.h5.getDescription()));
		hc6Text.getChildren().add(new Text(ConstraintType.h6.getDescription()));
		hc7Text.getChildren().add(new Text(ConstraintType.h7.getDescription()));
		hc8Text.getChildren().add(new Text(ConstraintType.h8.getDescription()));
		hc9Text.getChildren().add(new Text(ConstraintType.h9.getDescription()));
		hc10Text.getChildren().add(new Text(ConstraintType.h10.getDescription()));

		sc1Title.setText("Soft Constraint 1" + " (" + String.valueOf(getModel().getConstrWeightings().getS1()) + ") ");
		sc2Title.setText("Soft Constraint 2" + " (" + String.valueOf(getModel().getConstrWeightings().getS2()) + ") ");
		sc3Title.setText("Soft Constraint 3" + " (" + String.valueOf(getModel().getConstrWeightings().getS3()) + ") ");
		sc4Title.setText("Soft Constraint 4" + " (" + String.valueOf(getModel().getConstrWeightings().getS4()) + ") ");
		sc5Title.setText("Soft Constraint 5" + " (" + String.valueOf(getModel().getConstrWeightings().getS5()) + ") ");
		sc6Title.setText("Soft Constraint 6" + " (" + String.valueOf(getModel().getConstrWeightings().getS6()) + ") ");
		sc7Title.setText("Soft Constraint 7" + " (" + String.valueOf(getModel().getConstrWeightings().getS7()) + ") ");

		sc1Text.getChildren().add(new Text(ConstraintType.s1.getDescription()));
		sc2Text.getChildren().add(new Text(ConstraintType.s2.getDescription()));
		sc3Text.getChildren().add(new Text(ConstraintType.s3.getDescription()));
		sc4Text.getChildren().add(new Text(ConstraintType.s4.getDescription()));
		sc5Text.getChildren().add(new Text(ConstraintType.s5.getDescription()));
		sc6Text.getChildren().add(new Text(ConstraintType.s6.getDescription()));
		sc7Text.getChildren().add(new Text(ConstraintType.s7.getDescription()));

		nbs1Text.getChildren()
				.add(new Text("Chooses a single lecture at random and moves it to a new random feasible timeslot"));
		nbs2Text.getChildren().add(new Text(
				"Selects two periods at random and swaps all the lectures in one period with all the lectures in the other period."));
		nbs3Text.getChildren()
				.add(new Text("Tries to select two random assignments that are swapable and swaps them."));
		nbs4Text.getChildren().add(new Text(
				"Chooses a sample of 10% of assignments at random and moves the two assignments with the highest penalty out of them to a new random feasible timeslot.\n"));
		nbs5Text.getChildren().add(new Text(
				"Chooses a sample of 20% of assignments at random and moves the two assignments with the highest penalty out of them to a new random feasible timeslot."));
	}

}
