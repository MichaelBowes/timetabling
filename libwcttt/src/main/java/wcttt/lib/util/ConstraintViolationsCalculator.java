/*
 * WCT³ (WIAI Course Timetabling Tool) is a software that strives to automate
 * the timetabling process at the WIAI faculty of the University of Bamberg.
 *
 * libwcttt comprises the data model, a binder (parser + emitter) to store the
 * data as XML files, the implementations of the algorithms as well as
 * functionality to calculate conflicts and their violations.
 *
 * Copyright (C) 2018 Nicolas Gross
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package wcttt.lib.util;

import wcttt.lib.model.*;

import java.util.*;

/**
 * Provides functionality to calculate the number violated soft and hard
 * constraints for a timetable.
 */
public class ConstraintViolationsCalculator {

	private Semester semester;

	public ConstraintViolationsCalculator(Semester semester) {
		this.semester = semester;
	}

	/**
	 * This method checks for a timetable assignment whether it would violate any
	 * hard constraints if it would be added to the timetable. Thus, the method is
	 * used BEFORE the assignment is added to the timetable.
	 *
	 * @param timetable  the timetable that should be checked.
	 * @param period     the period to which the assignment should be assigned.
	 * @param assignment the new assignment that should be checked.
	 * @return a list containing every hard constraint as many times as it would
	 *         have been violated by the assignment.
	 */
	public List<Tuple<ConstraintType, String>> calcAssignmentHardViolations(Timetable timetable, TimetablePeriod period,
			TimetableAssignment assignment) {
		List<Tuple<ConstraintType, String>> hardViolations = new LinkedList<Tuple<ConstraintType, String>>();

		if (assignment.getSession().isLecture()) {
			hardViolations.addAll(h1ViolationCount(period, assignment));
		} else {
			hardViolations.addAll(h2ViolationCount(period, assignment));
		}

		hardViolations.addAll(h3ViolationCount(period, assignment));

		if (assignment.getSession().isLecture()) {
			hardViolations.addAll(h4ViolationCount(period, assignment));
		} else {
			hardViolations.addAll(h5ViolationCount(period, assignment));
		}

		hardViolations.addAll(h6ViolationCount(period, assignment));

		Tuple<ConstraintType, String> h7Violation = h7ViolationCount(period, assignment);
		if (h7Violation != null) {
			hardViolations.add(h7Violation);
		}

		if (assignment.getSession().isLecture()) {
			Tuple<ConstraintType, String> h8Violation = h8ViolationCount(timetable, period, assignment);
			if (h8Violation != null) {
				hardViolations.add(h8Violation);
			}
		}

		Tuple<ConstraintType, String> h9Violation = h9ViolationCount(period, assignment);
		if (h9Violation != null) {
			hardViolations.add(h9Violation);
		}

		Tuple<ConstraintType, String> h10Violation = h10ViolationCount(assignment);
		if (h10Violation != null) {
			hardViolations.add(h10Violation);
		}

		return hardViolations;
	}

	/**
	 * Calls all calcSoftConstraints-Methods to create a Hashmap with all soft
	 * constraints and violations of them in it
	 * 
	 * @param timetable
	 * @return a Hashmap with all seven soft constraints + count of violations
	 */
	public Map<ConstraintType, Integer> calcTimetablePenalty(Timetable timetable) {
		Map<ConstraintType, Integer> violations = new HashMap<ConstraintType, Integer>();

		violations.put(ConstraintType.s6, s6ViolationCount(timetable));

		for (Course course : semester.getCourses()) {
			for (Map.Entry<ConstraintType, Integer> entry : calcCourseSoftViolations(course, timetable).entrySet()) {
				if (violations.containsKey(entry.getKey())) {
					violations.put(entry.getKey(), violations.get(entry.getKey()) + entry.getValue());
				} else {
					violations.put(entry.getKey(), entry.getValue());
				}
			}
		}

		for (Curriculum curriculum : semester.getCurricula()) {
			for (Map.Entry<ConstraintType, Integer> entry : calcCurriculumSoftViolations(curriculum, timetable)
					.entrySet()) {
				if (violations.containsKey(entry.getKey())) {
					violations.put(entry.getKey(), violations.get(entry.getKey()) + entry.getValue());
				} else {
					violations.put(entry.getKey(), entry.getValue());
				}
			}
		}

		for (TimetableDay day : timetable.getDays()) {
			for (TimetablePeriod period : day.getPeriods()) {
				for (TimetableAssignment assignment : period.getAssignments()) {
					for (Map.Entry<ConstraintType, Integer> entry : calcAssignmentSoftViolations(period, assignment,
							timetable).entrySet()) {
						if (violations.containsKey(entry.getKey())) {
							violations.put(entry.getKey(), violations.get(entry.getKey()) + entry.getValue());
						} else {
							violations.put(entry.getKey(), entry.getValue());
						}
					}
				}
			}
		}
		return violations;
	}

	/**
	 * Calculates the penalty value out of a Hashmap filled with all soft
	 * constraints violations
	 * 
	 * @param violations
	 * @return a double value that represents the penalty of the timetable
	 */
	public double calculateViolationsPenalty(Map<ConstraintType, Integer> violations) {
		double penalty = 0.0;
		for (Map.Entry<ConstraintType, Integer> violation : violations.entrySet()) {
			double weighting = 0.0;
			weighting = semester.getConstrWeightings().getWeighting(violation.getKey()) * violation.getValue();				
			penalty += weighting;
		}

		return penalty;
	}

	/**
	 * Fills a new Hashmap with soft constraints 2 and 4 and their count of
	 * penalties
	 * 
	 * @param course
	 * @param timetable
	 * @return the new Hashmap
	 */
	private Map<ConstraintType, Integer> calcCourseSoftViolations(Course course, Timetable timetable) {
		Map<ConstraintType, Integer> violations = new HashMap<ConstraintType, Integer>();

		violations.put(ConstraintType.s2, s2ViolationCount(course, timetable));

		violations.put(ConstraintType.s4, s4ViolationCount(course, timetable));

		return violations;
	}

	/**
	 * Fills a new Hashmap with soft constraint 7 and its count of penalties
	 * 
	 * @param curriculum
	 * @param timetable
	 * @return the new Hashmap
	 */
	private Map<ConstraintType, Integer> calcCurriculumSoftViolations(Curriculum curriculum, Timetable timetable) {
		Map<ConstraintType, Integer> violations = new HashMap<ConstraintType, Integer>();

		violations.put(ConstraintType.s7, s7ViolationCount(curriculum, timetable));

		return violations;
	}

	/**
	 * Fills a new Hashmap with soft constraints 1, 3 and 5 and their count of
	 * penalties
	 * 
	 * @param period
	 * @param assignment
	 * @param timetable
	 * @return the new Hashmap
	 */
	public Map<ConstraintType, Integer> calcAssignmentSoftViolations(TimetablePeriod period,
			TimetableAssignment assignment, Timetable timetable) {
		Map<ConstraintType, Integer> violations = new HashMap<ConstraintType, Integer>();

		violations.put(ConstraintType.s1, s1ViolationCount(assignment));

		violations.put(ConstraintType.s3, s3ViolationCount(period, assignment, timetable));

		violations.put(ConstraintType.s5, s5ViolationCount(period, assignment));

		return violations;
	}

	private List<Tuple<ConstraintType, String>> h1ViolationCount(TimetablePeriod period,
			TimetableAssignment assignment) {
		List<Tuple<ConstraintType, String>> constraintViloations = new LinkedList<Tuple<ConstraintType, String>>();
		for (TimetableAssignment otherAssignment : period.getAssignments()) {
			if (!otherAssignment.equals(assignment)
					&& otherAssignment.getSession().getCourse().equals(assignment.getSession().getCourse())) {

				constraintViloations.add(new Tuple<ConstraintType, String>(ConstraintType.h1,
						"The lecture '" + assignment.getSession().getName() + "' is in the same " + "period as '"
								+ otherAssignment.getSession().getName() + "' from the same course."));
			}
		}
		return constraintViloations;
	}

	private List<Tuple<ConstraintType, String>> h2ViolationCount(TimetablePeriod period,
			TimetableAssignment assignment) {

		List<Tuple<ConstraintType, String>> constraintViloations = new LinkedList<Tuple<ConstraintType, String>>();
		for (TimetableAssignment otherAssignment : period.getAssignments()) {
			if (!otherAssignment.equals(assignment)
					&& otherAssignment.getSession().getCourse().equals(assignment.getSession().getCourse())
					&& otherAssignment.getSession().isLecture()) {

				constraintViloations.add(new Tuple<ConstraintType, String>(ConstraintType.h2,
						"The practical '" + assignment.getSession().getName()
								+ "' is in the same period as the lecture '" + otherAssignment.getSession().getName()
								+ "' of the same course."));
			}
		}
		return constraintViloations;
	}

	private List<Tuple<ConstraintType, String>> h3ViolationCount(TimetablePeriod period,
			TimetableAssignment assignment) {

		List<Tuple<ConstraintType, String>> constraintViloations = new LinkedList<Tuple<ConstraintType, String>>();
		for (TimetableAssignment otherAssignment : period.getAssignments()) {
			if (!otherAssignment.equals(assignment) && otherAssignment.getRoom().equals(assignment.getRoom())) {
				constraintViloations.add(new Tuple<ConstraintType, String>(ConstraintType.h3,
						"The practicals/lectures '" + assignment.getSession().getName() + "' and '"
								+ otherAssignment.getSession().getName() + "' were both assigned to the" + "same room ("
								+ otherAssignment.getRoom().getName() + ")."));
			}
		}
		return constraintViloations;
	}

	private List<Tuple<ConstraintType, String>> h4ViolationCount(TimetablePeriod period,
			TimetableAssignment assignment) {

		List<Tuple<ConstraintType, String>> constraintViloations = new LinkedList<Tuple<ConstraintType, String>>();
		for (Curriculum curriculum : semester.getCurricula()) {
			if (curriculum.getCourses().contains(assignment.getSession().getCourse())) {
				for (TimetableAssignment otherAssignment : period.getAssignments()) {
					// Check if course is contained in the curriculum
					if (curriculum.getCourses().contains(otherAssignment.getSession().getCourse())) {
						// If assignment is no lecture and has more than one practical it can be
						// ignored.
						if (otherAssignment.getSession().getPreAssignment().isPresent()
								&& assignment.getSession().getPreAssignment().isPresent()) {
							// If both assignments are preassigned.
							continue;
						} else if (otherAssignment.getSession().isLecture()) {
							constraintViloations.add(new Tuple<ConstraintType, String>(ConstraintType.h4,
									"The lecture '" + assignment.getSession().getName()
											+ "' is in the same period as the lecture '"
											+ otherAssignment.getSession().getName() + " of the same curruculum ("
											+ curriculum.getName() + ")."));
						} else if (otherAssignment.getSession().getCourse().getPracticals().size() == 1) {
							constraintViloations.add(new Tuple<ConstraintType, String>(ConstraintType.h4,
									"The lecture '" + assignment.getSession().getName() + "' is in the same period as '"
											+ otherAssignment.getSession().getName() + " of the same curruculum ("
											+ curriculum.getName() + "), which is the only practical in its course."));
						}
					}
				}
			}
		}
		return constraintViloations;
	}

	private List<Tuple<ConstraintType, String>> h5ViolationCount(TimetablePeriod period,
			TimetableAssignment assignment) {
		int numberOfCoursePracticals = assignment.getSession().getCourse().getPracticals().size();
		int numberOfCoursePracticalsInPeriod = 0;

		Course assignmentCourse = assignment.getSession().getCourse();

		List<Tuple<ConstraintType, String>> constraintViloations = new LinkedList<Tuple<ConstraintType, String>>();
		for (Curriculum curriculum : semester.getCurricula()) {
			if (curriculum.getCourses().contains(assignment.getSession().getCourse())) {
				// Curricular contains course of the assignment
				for (TimetableAssignment otherAssignment : period.getAssignments()) {
					// Check if all practicals of the assignment are in the same period
					if (otherAssignment.getSession().getCourse().equals(assignmentCourse)
							&& !assignment.getSession().isLecture()) {
						numberOfCoursePracticalsInPeriod++;
					}
					/*
					 * Curricular contains other course of an assignment in the selected period
					 */
					if (curriculum.getCourses().contains(otherAssignment.getSession().getCourse())) {
						if (otherAssignment.getSession().isLecture()) {
							// Other course is a lecture
							constraintViloations.add(new Tuple<ConstraintType, String>(ConstraintType.h5,
									"The practical '" + assignment.getSession().getName()
											+ "' is in the same period as the lecture '"
											+ otherAssignment.getSession().getName() + "' which is in the same "
											+ "curriculum (" + curriculum.getName() + ")."));
						} else if (otherAssignment.getSession().getCourse().getPracticals().size() == 1
								&& assignmentCourse.getPracticals().size() == 1) {
							// Both sessions are the only practicals of their course.
							constraintViloations.add(new Tuple<ConstraintType, String>(ConstraintType.h5,
									"The practical '" + assignment.getSession().getName()
											+ "' is in the same period as the practical '"
											+ otherAssignment.getSession().getName() + "' which is in the same "
											+ "curriculum (" + curriculum.getName() + "),"
											+ " and both are the only practicals in their course."));
						} else if (otherAssignment.getSession().getCourse().equals(assignmentCourse)) {
							// The other session is from the same course.
							constraintViloations.add(new Tuple<ConstraintType, String>(ConstraintType.h5,
									"The practical '" + assignment.getSession().getName()
											+ "' is in the same period as the practical '"
											+ otherAssignment.getSession().getName() + "' which is in the same "
											+ "course (" + otherAssignment.getSession().getCourse().getName() + "),"
											+ " and both are the only practicals in their course."));
						}
					}
				}
			}
		}
		// All practicals are overlapping
		if (numberOfCoursePracticals == numberOfCoursePracticalsInPeriod) {
			constraintViloations
					.add(new Tuple<ConstraintType, String>(ConstraintType.h5, "All practicals of the course '"
							+ assignment.getSession().getCourse().getName() + "' are in the same period."));
		}
		return constraintViloations;
	}

	private List<Tuple<ConstraintType, String>> h6ViolationCount(TimetablePeriod period,
			TimetableAssignment assignment) {

		List<Tuple<ConstraintType, String>> constraintViloations = new LinkedList<Tuple<ConstraintType, String>>();
		for (TimetableAssignment otherAssignment : period.getAssignments()) {
			if (!otherAssignment.equals(assignment)
					&& otherAssignment.getSession().getTeacher().equals(assignment.getSession().getTeacher())) {
				constraintViloations.add(new Tuple<ConstraintType, String>(ConstraintType.h6,
						"The assignment '" + assignment.getSession().getName() + "' and the assignment '"
								+ otherAssignment.getSession().getName()
								+ "' are both held by the same teacher in the same period (" + period.toString()
								+ ")."));
			}
		}
		return constraintViloations;
	}

	private Tuple<ConstraintType, String> h7ViolationCount(TimetablePeriod period, TimetableAssignment assignment) {

		for (Period unavailable : assignment.getSession().getTeacher().getUnavailablePeriods()) {
			if (unavailable.getDay() == period.getDay() && unavailable.getTimeSlot() == period.getTimeSlot()) {
				Tuple<ConstraintType, String> violation = new Tuple<ConstraintType, String>(ConstraintType.h7,
						"The teacher '" + assignment.getSession().getTeacher().getName() + "' for the session '"
								+ assignment.getSession().getName() + "' is unavailable in the period '"
								+ period.toString() + "'.");
				return violation;
			}
		}
		return null;
	}

	private Tuple<ConstraintType, String> h8ViolationCount(Timetable timetable, TimetablePeriod period,
			TimetableAssignment assignment) {

		TimetableDay day = timetable.getDays().get(period.getDay() - 1);
		for (TimetablePeriod otherPeriod : day.getPeriods()) {
			for (TimetableAssignment otherAssgmt : otherPeriod.getAssignments()) {
				if (otherAssgmt.getSession().getCourse().equals(assignment.getSession().getCourse())
						&& otherAssgmt.getSession().isLecture()
						&& !(otherAssgmt.getSession().equals(assignment.getSession())
								&& otherAssgmt.getSession().isDoubleSession())) {
					Tuple<ConstraintType, String> violation = new Tuple<ConstraintType, String>(ConstraintType.h8,
							"The lectures '" + assignment.getSession().getName() + "' and '"
									+ otherAssgmt.getSession().getName()
									+ "' are from the same course and held on the same day (" + day.toString() + ").");
					return violation;
				}
			}
		}
		return null;
	}

	public Tuple<ConstraintType, String> h9ViolationCount(TimetablePeriod period, TimetableAssignment assignment) {
		if (!assignment.getSession().getPreAssignment().isPresent()) {
			return null;
		}

		Period preAssignment = assignment.getSession().getPreAssignment().get();
		if (preAssignment.getDay() == period.getDay()
				&& (preAssignment.getTimeSlot() == period.getTimeSlot() || (assignment.getSession().isDoubleSession()
						&& (preAssignment.getTimeSlot() + 1) == period.getTimeSlot()))) {
			return null;
		} else {
			Tuple<ConstraintType, String> violation = new Tuple<ConstraintType, String>(ConstraintType.h9,
					"The preassigned session '" + assignment.getSession().getName()
							+ "' is not in its preassigned timeslot (" + preAssignment.toString() + ").");
			return violation;
		}
	}

	public Tuple<ConstraintType, String> h10ViolationCount(TimetableAssignment assignment) {
		if (assignment.getRoom() instanceof InternalRoom && assignment.getSession() instanceof InternalSession
				&& ((InternalRoom) assignment.getRoom()).getFeatures()
						.compareTo(((InternalSession) assignment.getSession()).getRoomRequirements()) < 0) {
			Tuple<ConstraintType, String> violation = new Tuple<ConstraintType, String>(ConstraintType.h10,
					"The features of the room '" + assignment.getRoom().getName()
							+ "' don't match the requirements of the session '" + assignment.getSession().getName()
							+ "' which takes place in it.");
			return violation;
		} else {
			return null;
		}
	}

	/**
	 * Calculates all violations of soft constraint one
	 * 
	 * @param assignment
	 * @return - <code>(0)</code> if no violation occurs<br>
	 *         - <code>(requiredCapacity - actualCapacity)</code> if violated
	 */
	public int s1ViolationCount(TimetableAssignment assignment) {
		if (assignment.getRoom() instanceof InternalRoom && assignment.getSession() instanceof InternalSession) {
			int actualRoomCapacity = ((InternalRoom) assignment.getRoom()).getCapacity();
			int requiredRoomCapacity = ((InternalSession) assignment.getSession()).getStudents();
			if (actualRoomCapacity > requiredRoomCapacity) {
				return 0;
			} else {
				return requiredRoomCapacity - actualRoomCapacity;
			}
		} else {
			return 0;
		}
	}

	/**
	 * Calculates all violations of soft constraint two
	 * 
	 * @param course
	 * @param timetable
	 * @return - <code>(0)</code> if no violation occurs<br>
	 *         - <code>(difference)</code> if violated
	 */
	public int s2ViolationCount(Course course, Timetable timetable) {
		int earliestDay = ValidationHelper.PERIOD_DAY_MIN;
		int lastDay = ValidationHelper.DAYS_PER_WEEK_MAX;
		for (TimetableDay day : timetable.getDays()) {
			for (TimetablePeriod period : day.getPeriods()) {
				for (TimetableAssignment assignment : period.getAssignments()) {
					if (assignment.getSession().getCourse().equals(course) && assignment.getSession().isLecture()) {
						if (period.getDay() < earliestDay) {
							earliestDay = period.getDay();
						} else if (period.getDay() > lastDay) {
							lastDay = period.getDay();
						}
					}
				}
			}
		}
		int difference = course.getMinNumberOfDays() - ((lastDay - earliestDay) + 1);
		if (difference > 0) {
			return difference;
		} else {
			return 0;
		}
	}

	/**
	 * Calculates all violations of soft constraint three
	 * 
	 * @param period
	 * @param assignment
	 * @param timetable
	 * @return an integer value that represents the number of violations occured
	 */
	public int s3ViolationCount(TimetablePeriod period, TimetableAssignment assignment, Timetable timetable) {
		if (!assignment.getSession().isLecture() || semester.getTimeSlotsPerDay() == 1) {
			return 0;
		}
		int counter = 0;
		for (Curriculum curriculum : semester.getCurricula()) {
			if (curriculum.getCourses().contains(assignment.getSession().getCourse())) {
				boolean foundAdjacent = false;
				if (period.getTimeSlot() > ValidationHelper.PERIOD_TIME_SLOT_MIN) {
					TimetablePeriod before = timetable.getDays().get(period.getDay() - 1).getPeriods()
							.get(period.getTimeSlot() - 2);
					for (TimetableAssignment assBefore : before.getAssignments()) {
						if (assBefore.getSession().isLecture()
								&& curriculum.getCourses().contains(assBefore.getSession().getCourse())) {
							foundAdjacent = true;
							break;
						}
					}
				}
				if (!foundAdjacent && period.getTimeSlot() < semester.getTimeSlotsPerDay()) {
					TimetablePeriod after = timetable.getDays().get(period.getDay() - 1).getPeriods()
							.get(period.getTimeSlot());
					for (TimetableAssignment assAfter : after.getAssignments()) {
						if (assAfter.getSession().isLecture()
								&& curriculum.getCourses().contains(assAfter.getSession().getCourse())) {
							foundAdjacent = true;
							break;
						}
					}
				}
				if (!foundAdjacent) {
					counter++;
				}
			}
		}
		return counter;
	}

	/**
	 * Calculates all violations of soft constraint four
	 * 
	 * @param course
	 * @param timetable
	 * @return an integer value that represents the number of violations occured
	 */
	public int s4ViolationCount(Course course, Timetable timetable) {
		int counter = 0;
		Room room = null;
		for (TimetableDay day : timetable.getDays()) {
			for (TimetablePeriod period : day.getPeriods()) {
				for (TimetableAssignment assgmt : period.getAssignments()) {
					if (assgmt.getSession().getCourse().equals(course) && assgmt.getSession().isLecture()) {
						if (room == null) {
							room = assgmt.getRoom();
						} else if (!room.equals(assgmt.getRoom())) {
							counter++;
						}
					}
				}
			}
		}
		return counter;
	}

	/**
	 * Calculates all violations of soft constraint five
	 * 
	 * @param period
	 * @param assignment
	 * @return an integer value that represents the number of violations occured
	 */
	public int s5ViolationCount(TimetablePeriod period, TimetableAssignment assignment) {
		int counter = 0;
		for (Period unfavorable : assignment.getSession().getTeacher().getUnfavorablePeriods()) {
			if (unfavorable.getDay() == period.getDay() && unfavorable.getTimeSlot() == period.getTimeSlot()) {
				counter++;
			}
		}
		return counter;
	}

	/**
	 * Calculates all violations of soft constraint six
	 * 
	 * @param timetable
	 * @return an integer value that represents the number of violations occured
	 */
	public int s6ViolationCount(Timetable timetable) {
		int counter = 0;
		for (TimetableDay day : timetable.getDays()) {
			List<List<Teacher>> activeTeachers = new ArrayList<>(semester.getTimeSlotsPerDay());
			for (TimetablePeriod period : day.getPeriods()) {
				List<Teacher> activeInTimeSlot = new LinkedList<>();
				for (TimetableAssignment assgmt : period.getAssignments()) {
					activeInTimeSlot.add(assgmt.getSession().getTeacher());
				}
				activeTeachers.add(activeInTimeSlot);
			}
			for (int i = 0; i < activeTeachers.size() - 2; i++) {
				for (Teacher teacher : activeTeachers.get(i)) {
					for (int j = i + 1; j < activeTeachers.size(); j++) {
						if (!activeTeachers.get(j).contains(teacher)) {
							if (j - i > 2) {
								counter += j - i;
								for (int k = i + 1; k < j; k++) {
									activeTeachers.get(k).remove(teacher);
								}
							}
							break;
						} else if (j == activeTeachers.size() - 1 && activeTeachers.get(j).contains(teacher)) {
							if ((j - i) + 1 > 2) {
								counter += (j - i) + 1;
							}
						}
					}

				}
			}
		}
		return counter;
	}

	/**
	 * Calculates all violations of soft constraint seven
	 * 
	 * @param curriculum
	 * @param timetable
	 * @return an integer value that represents the number of violations occured
	 */
	public int s7ViolationCount(Curriculum curriculum, Timetable timetable) {
		int counter = 0;
		for (TimetableDay day : timetable.getDays()) {
			int lecturesPerDay = 0;
			for (TimetablePeriod period : day.getPeriods()) {
				for (TimetableAssignment assgmt : period.getAssignments()) {
					if (curriculum.getCourses().contains(assgmt.getSession().getCourse())
							&& assgmt.getSession().isLecture()) {
						lecturesPerDay++;
					}
				}
			}
			if (lecturesPerDay > semester.getMaxDailyLecturesPerCur()) {
				counter += lecturesPerDay - semester.getMaxDailyLecturesPerCur();
			}
		}
		return counter;
	}
}
