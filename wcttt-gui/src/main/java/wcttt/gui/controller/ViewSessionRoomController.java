package wcttt.gui.controller;

import java.util.ArrayList;
import java.util.List;

import javafx.application.HostServices;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import wcttt.gui.model.Model;
import wcttt.lib.model.Course;
import wcttt.lib.model.InternalRoom;
import wcttt.lib.model.InternalSession;
import wcttt.lib.model.Session;
import wcttt.lib.util.SessionRoomConflict;

/**
 * 
 * Controller for the window displaying the Session-Room Matrix, which shows the
 * relation of each room's available features to the requirements of the
 * sessions.
 * 
 * @author Michael Bowes
 *
 */

public class ViewSessionRoomController extends Controller {

	@FXML
	private ScrollPane scroll;

	@FXML
	private GridPane matrix;

	@FXML
	private BorderPane rootPane;

	@FXML
	private TextField xAxisName;

	@FXML
	private TextField yAxisName;

	@FXML
	protected void initialize() {

		scroll = new ScrollPane(matrix);
		scroll.setVbarPolicy(ScrollBarPolicy.ALWAYS);
		scroll.setHbarPolicy(ScrollBarPolicy.ALWAYS);
		rootPane.setCenter(scroll);
	}

	private List<InternalSession> getSessions() {

		List<InternalSession> sessions = new ArrayList<>();

		for (Course course : getModel().getSemester().getCourses()) {

			for (Session lecture : course.getLectures()) {
				if (lecture instanceof InternalSession) {
					sessions.add((InternalSession) lecture);
				}
			}
			for (Session practical : course.getPracticals()) {
				if (practical instanceof InternalSession) {
					sessions.add((InternalSession) practical);
				}
			}
		}

		return sessions;
	}

	/**
	 * Generates the session-room matrix.
	 */
	private void generateMatrix() {

		List<InternalSession> sessions = getSessions();

		int x = 0;
		for (InternalRoom room : getModel().getInternalRooms()) {

			double number = 1 / getModel().getTeachers().size();
			TextField sessionColumnTextField = new TextField(room.getName());
			sessionColumnTextField.setEditable(false);
			sessionColumnTextField.maxWidth(200);
			sessionColumnTextField.setMouseTransparent(true);
			ColumnConstraints col1 = new ColumnConstraints();
			col1.setPercentWidth(number);
			RowConstraints col2 = new RowConstraints();
			col2.setPercentHeight(number);
			matrix.getColumnConstraints().addAll(col1);
			matrix.getRowConstraints().addAll(col2);
			matrix.add(sessionColumnTextField, x + 1, 0);

			int y = 0;
			for (InternalSession session : sessions) {

				TextField sessionRowTextField = new TextField(session.getName());
				sessionRowTextField.setEditable(false);
				sessionRowTextField.setMouseTransparent(true);
				matrix.add(sessionRowTextField, 0, y + 1);

				int conflictType = 0;
				SessionRoomConflict conflict = new SessionRoomConflict();
				conflict.setCapacityDeviation(room.getCapacity() - session.getStudents());
				if (session.getRoomRequirements().compareTo(room.getFeatures()) <= 0) {
					conflict.setFullfillsFeatures(true);
				}

				if (!conflict.fullfillsFeatures() | conflict.getCapacityDeviation() < 0) {
					conflictType = 2;
				} else if (conflict.getCapacityDeviation() > 20) {
					conflictType = 1;
				}

				Circle circle = drawCircle(conflictType, conflict);
				Tooltip.install(circle, createConflictTooltip(room, session, conflict));
				matrix.add(circle, x + 1, y + 1);
				GridPane.setHalignment(circle, HPos.CENTER);
				y++;
			}
			x++;
		}
	}

	/**
	 * Draws a circle with attached tooltip for the specific conflict.
	 * 
	 * @param conflictType
	 * @return
	 */
	private Circle drawCircle(int conflictType, SessionRoomConflict conflict) {
		Circle circle = new Circle();
		circle.setFill(getConflictColor(conflictType, conflict));
		circle.setRadius(25);
		circle.setCenterX(0);
		circle.setCenterY(0);

		return circle;
	}

	/**
	 * Creates tooltip for hover over the conflict circles.
	 * 
	 * @param conflictType
	 * @return
	 */
	private Tooltip createConflictTooltip(InternalRoom room, Session session, SessionRoomConflict conflict) {
		Tooltip tip = new Tooltip();

		String deviationText;
		int deviation = conflict.getCapacityDeviation();
		if (deviation < 0) {
			deviationText = "Capacity: " + Math.abs(deviation) + " slots missing";
		} else {
			deviationText = "Capacity: " + Math.abs(deviation) + " slots unused";
		}

		String str = room.getName() + " - " + session.getName() + "\n" + "Fulfills Features: "
				+ conflict.fullfillsFeatures() + "\n" + deviationText;
		tip.setText(str);
		tip.prefWidth(100);
		tip.setWrapText(true);
		tip.setHideOnEscape(true);
		tip.setStyle("-fx-background-color: linear-gradient(#E4EAA2, #9CD672);");

		return tip;
	}

	/**
	 * 
	 * Chooses either white, yellow or red as Paint color depending on whether the
	 * conflict is non-existant, soft or hard constraint.
	 * 
	 * @param conflict
	 * @return
	 */
	private Paint getConflictColor(int conflictType, SessionRoomConflict conflict) {
		Color color = Color.GREEN;
		
		switch (conflictType) {
		case 0:
			color = Color.GREEN;
			break;
		case 1:
			color = Color.YELLOW;
			break;
		case 2:
			color = Color.RED;
			break;
		}
		return color;
	}
	
	@Override
	public void setup(Stage stage, HostServices hostServices, MainController mainController, Model model) {
		super.setup(stage, hostServices, mainController, model);
		generateMatrix();
	}

}
