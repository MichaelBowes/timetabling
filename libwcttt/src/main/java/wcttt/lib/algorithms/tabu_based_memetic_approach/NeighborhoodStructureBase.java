/*
 * WCT³ (WIAI Course Timetabling Tool) is a software that strives to automate
 * the timetabling process at the WIAI faculty of the University of Bamberg.
 *
 * libwcttt comprises the data model, a binder (parser + emitter) to store the
 * data as XML files, the implementations of the algorithms as well as
 * functionality to calculate conflicts and their violations.
 *
 * Copyright (C) 2018 Nicolas Gross
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package wcttt.lib.algorithms.tabu_based_memetic_approach;

import wcttt.lib.algorithms.WctttAlgorithmException;
import wcttt.lib.model.Semester;
import wcttt.lib.model.Timetable;
import wcttt.lib.util.ConstraintViolationsCalculator;

/**
 * Defines the functionality of a neighborhood structure.
 */
public abstract class NeighborhoodStructureBase {

	protected final ConstraintViolationsCalculator constrCalc;
	private static boolean generateStatisticData = false;
	
	public NeighborhoodStructureBase(Semester semester) {
		this.constrCalc = new ConstraintViolationsCalculator(semester);
	}
	
	/**
	 * Applies the {@link NeighborhoodStructureBase} to a given timetable.
	 * If the generation of statistical data is enabled, it will be saved in the {@link Timetable}.
	 * @param timetable - Timetable.
	 * @param semester - Semester containing the timetable.
	 * @throws WctttAlgorithmException - If an error occurred while trying to apply the {@link NeighborhoodStructureBase}.
	 */
	public final void apply(Timetable timetable, Semester semester) throws WctttAlgorithmException{
		applyImpl(timetable, semester);
		if(generateStatisticData) {
			updateTimetable(timetable, this);
		}
	}
	
	protected abstract void applyImpl(Timetable timetable, Semester semester)
			throws WctttAlgorithmException;
	
	/**
	 * Returns the description for the {@link NeighborhoodStructureBase}.
	 * @return
	 */
	public abstract String getDescription();
	
	/**
	 * Returns the identifying name of the Structure.
	 * @return
	 */
	public abstract String getName();
	
	/**
	 * Calculates the difference in penalty created by this structure and saves
	 * the data in the {@link Timetable}.
	 * @param timetable
	 * @param nbs
	 */
	protected void updateTimetable(Timetable timetable, NeighborhoodStructureBase nbs) {
		double oldPenalty = timetable.getSoftConstraintPenalty();
		timetable.updateSoftConstraintPenalty(constrCalc);		
		timetable.incrementNbsCount(nbs);
		double delta = timetable.getSoftConstraintPenalty() - oldPenalty;
		timetable.updatePenaltyDelta(nbs, delta);		
	}
	
	public static void setGenerateStatisticData(boolean value) {
		generateStatisticData = value;
	}
	
	public static boolean getGenerateStatisticData() {
		return generateStatisticData;
	}
}
