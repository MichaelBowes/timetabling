/*
 * WCT³ (WIAI Course Timetabling Tool) is a software that strives to automate
 * the timetabling process at the WIAI faculty of the University of Bamberg.
 *
 * libwcttt comprises the data model, a binder (parser + emitter) to store the
 * data as XML files, the implementations of the algorithms as well as
 * functionality to calculate conflicts and their violations.
 *
 * Copyright (C) 2018 Nicolas Gross
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package wcttt.lib.algorithms.tabu_based_memetic_approach;

import wcttt.lib.algorithms.WctttAlgorithmException;
import wcttt.lib.algorithms.WctttAlgorithmFatalException;
import wcttt.lib.model.*;
import wcttt.lib.util.TimetableUtil;

import java.util.Collections;
import java.util.List;
import java.util.Random;

public class NeighborhoodStructure1 extends NeighborhoodStructureBase {

	private static final String NAME = "Nbs1";
	private static final String DESCRIPTION = "Chooses a single lecture at random and"
			+ " moves it to a new random feasible timeslot.";
	
	public NeighborhoodStructure1(Semester semester) {
		super(semester);
	}
	/**
	 * Chooses a single lecture at random and moves it to a new random feasible
	 * timeslot.
	 *
	 * @param timetable the timetable to which the neighborhood structure should
	 *                  be applied.
	 * @param semester the semester the timetable belongs to.
	 */
	@Override
	protected void applyImpl(Timetable timetable, Semester semester)
			throws WctttAlgorithmException {
		TimetableAssignment randomAssgmt;
		do {
			randomAssgmt = selectRandomAssignment(timetable);
		} while (randomAssgmt.getSession().getPreAssignment().isPresent());
		
		TimetableUtil.removeSessionAssignments(timetable, randomAssgmt);
		
		// Because external sessions must have a pre-assignment:	
		assert randomAssgmt.getSession() instanceof InternalSession;
		assert randomAssgmt.getRoom() instanceof InternalRoom;

		List<Period> periods = Util.createPeriodList(semester);
		Collections.shuffle(periods);

		if (!Util.assignSessionRandomly(
				(InternalSession) randomAssgmt.getSession(), timetable,
				semester, periods, null, null, null, null)) {
			throw new WctttAlgorithmFatalException("Implementation error, " +
					"could not find suitable room and period for new " +
					"assignment, at least the period and room of the previous" +
					" assignment should have been found");
		}
	}

	private TimetableAssignment selectRandomAssignment(Timetable timetable) {
		Random random = new Random();
		TimetableDay randomDay;
		TimetablePeriod randomPeriod;
		do {
			randomDay = timetable.getDays().get(
					random.nextInt(timetable.getDays().size()));
			randomPeriod = randomDay.getPeriods().get(
					random.nextInt(randomDay.getPeriods().size()));
		} while (randomPeriod.getAssignments().isEmpty());
		return randomPeriod.getAssignments().get(
				random.nextInt(randomPeriod.getAssignments().size()));
	}

	@Override
	public String getDescription() {
		return DESCRIPTION;
	}

	@Override
	public String getName() {
		return NAME;
	}
}
