package wcttt.lib.algorithms.tabu_based_memetic_approach;

import java.util.List;
import java.util.Random;

import wcttt.lib.algorithms.WctttAlgorithmException;
import wcttt.lib.model.ConstraintType;
import wcttt.lib.model.Room;
import wcttt.lib.model.Semester;
import wcttt.lib.model.TimeSlot;
import wcttt.lib.model.Timetable;
import wcttt.lib.model.TimetableAssignment;
import wcttt.lib.model.TimetableDay;
import wcttt.lib.model.TimetablePeriod;
import wcttt.lib.model.WctttModelException;
import wcttt.lib.util.TimetableUtil;
import wcttt.lib.util.Tuple;

/**
 * @author Nicolas Bruch
 *
 */
public class NeighborhoodStructure3 extends NeighborhoodStructureBase {

	private static final String NAME = "Nbs3";
	private static final String DESCRIPTION = "Tries to select two random "
			+ "assignments that are swapable and swaps them.";
	private static final int TRIES = 100;

	public NeighborhoodStructure3(Semester semester) {
		super(semester);
	}
	/*
	 * Tries to select two random assignments that are swapable and swaps them.
	 *
	 */
	@Override
	protected void applyImpl(Timetable timetable, Semester semester) throws WctttAlgorithmException {
		Tuple<TimeSlot, TimeSlot> timeSlots = null;
		try {
			timeSlots = getSwappableTimeSlots(timetable, semester);
		} catch (WctttModelException e) {
			e.printStackTrace();
			throw new WctttAlgorithmException(e);
		}
		if (timeSlots == null) {
			return;
		}
		TimeSlot a = timeSlots.x;
		TimeSlot b = timeSlots.y;
		try {
			TimetableUtil.swapTimeslots(timetable, a.getAssignment(), a.getDay(), a.getPeriod(), a.getRoom(),
					b.getAssignment(), b.getDay(), b.getPeriod(), b.getRoom());
		} catch (WctttModelException e) {
			e.printStackTrace();
			throw new WctttAlgorithmException(e);
		}

	}

	private Tuple<TimeSlot, TimeSlot> getSwappableTimeSlots(
			Timetable timetable, Semester semester) throws WctttModelException, WctttAlgorithmException{
		
		Random random = new Random();
		TimetablePeriod periodA;
		TimetablePeriod periodB;
		TimetableDay dayA;
		TimetableDay dayB;
		Room roomA;
		Room roomB;
		TimetableAssignment assignmentA = null;
		TimetableAssignment assignmentB = null;		

		int counter = 0;
		do {
			counter++;
			periodA = null;
			periodB = null;
			dayA = null;
			dayA = null;
			roomA = null;
			roomB = null;
			assignmentA = null;
			assignmentB = null;
			
			int internalCounter = 0;
			do {
				dayA = timetable.getDays().get(random.nextInt(timetable.getDays().size()));
				periodA = dayA.getPeriods().get(random.nextInt(dayA.getPeriods().size()));
				roomA = semester.getInternalRooms().get(random.nextInt(semester.getInternalRooms().size()));
				//Get assignment A
				for(TimetableAssignment assignment : periodA.getAssignments()) {
					if(assignment.getRoom().getName().equals(roomA.getName())) {
						assignmentA = assignment;
					}
				}
				internalCounter++;
			}while(assignmentA == null || internalCounter == 50);
			if(internalCounter == 50) {
				return null;
			}
			internalCounter = 0;
			do {
				dayB = timetable.getDays().get(random.nextInt(timetable.getDays().size()));			
				periodB = dayB.getPeriods().get(random.nextInt(dayB.getPeriods().size()));			
				roomB = semester.getInternalRooms().get(random.nextInt(semester.getInternalRooms().size()));
				//Get assignment B
				for(TimetableAssignment assignment : periodB.getAssignments()) {
					if(assignment.getRoom().getName().equals(roomB.getName())) {
						assignmentB = assignment;
					}
				}
				internalCounter++;
			}while(assignmentB == null || internalCounter == 50);
			if(internalCounter == 50) {
				return null;
			}
			if(!assignmentA.getSession().equals(assignmentB.getSession())) {
				//Both slots contain assignments
				Tuple<Double, List<Tuple<ConstraintType, String>>> violation = 
						TimetableUtil.getViolations(semester, timetable,
								assignmentA, dayA, periodA, roomA,
								assignmentB, dayB, periodB, roomB);
					
				//If swapping the periods won't cause any violations.
				if(violation.y.isEmpty()) {
					TimeSlot slot1 = new TimeSlot(dayA, periodA, roomA);
					slot1.setAssignment(assignmentA);
					TimeSlot slot2 = new TimeSlot(dayB, periodB, roomB);
					slot2.setAssignment(assignmentB);
					return new Tuple<TimeSlot, TimeSlot>(slot1, slot2);	
				}
			}			
		}while(counter < TRIES);
		return null;
	}

	@Override
	public String getDescription() {
		return DESCRIPTION;
	}

	@Override
	public String getName() {
		return NAME;
	}
}
