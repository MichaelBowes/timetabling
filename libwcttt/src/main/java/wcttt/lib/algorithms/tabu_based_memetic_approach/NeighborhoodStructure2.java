/*
 * WCT³ (WIAI Course Timetabling Tool) is a software that strives to automate
 * the timetabling process at the WIAI faculty of the University of Bamberg.
 *
 * libwcttt comprises the data model, a binder (parser + emitter) to store the
 * data as XML files, the implementations of the algorithms as well as
 * functionality to calculate conflicts and their violations.
 *
 * Copyright (C) 2018 Nicolas Gross
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package wcttt.lib.algorithms.tabu_based_memetic_approach;

import wcttt.lib.algorithms.WctttAlgorithmFatalException;
import wcttt.lib.model.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class NeighborhoodStructure2 extends NeighborhoodStructureBase {
	
	private static final String NAME = "Nbs2";
	private static final String DESCRIPTION = "Selects two periods at random and simply swap all the lectures in one\n" + 
			"	 * period with all the lectures in the other period.";

	public NeighborhoodStructure2(Semester semester) {
		super(semester);
	}
	/**
	 * Selects two periods at random and simply swap all the lectures in one
	 * period with all the lectures in the other period.
	 *
	 * @param timetable the timetable to which the neighborhood structure should be
	 *                  applied.
	 * @param semester  the semester the timetable belongs to.
	 */
	@Override
	protected void applyImpl(Timetable timetable, Semester semester) {
		TimetablePeriod[] randomPeriods = selectSuitablePeriods(timetable);

		if (randomPeriods == null) {
			// Could not find a suitable pair of periods, do nothing
			return;
		}

		TimetableDay dayA = timetable.getDays().get(randomPeriods[0].getDay() - 1);
		TimetableDay dayB = timetable.getDays().get(randomPeriods[1].getDay() - 1);

		try {
			swapPeriods(dayA, dayB, randomPeriods[0], randomPeriods[1]);
		} catch (WctttModelException e) {
			throw new WctttAlgorithmFatalException("Implementation error", e);
		}
	}

	/**
	 * Swaps all assignments of two periods that are not preassigned.
	 * 
	 * @param dayA    - Day of the first period.
	 * @param dayB    - Day of the second period.
	 * @param periodA - First period.
	 * @param periodB - Second period.
	 * @throws WctttModelException - If an assignment could not be added to the
	 *                             other period.
	 */
	private void swapPeriods(TimetableDay dayA, TimetableDay dayB, TimetablePeriod periodA, TimetablePeriod periodB)
			throws WctttModelException {
		List<TimetableAssignment> periodAAssignments = new ArrayList<TimetableAssignment>();
		List<TimetableAssignment> periodBAssignments = new ArrayList<TimetableAssignment>();
		for (TimetableAssignment assignment : periodA.getAssignments()) {
			if (!assignment.getSession().getPreAssignment().isPresent()) {
				periodAAssignments.add(assignment);
			}
		}
		for (TimetableAssignment assignment : periodB.getAssignments()) {
			if (!assignment.getSession().getPreAssignment().isPresent()) {
				periodBAssignments.add(assignment);
			}
		}
		for (TimetableAssignment assignment : periodAAssignments) {
			periodB.addAssignment(assignment);
			periodA.removeAssignment(assignment);
		}
		for (TimetableAssignment assignment : periodBAssignments) {
			periodA.addAssignment(assignment);
			periodB.removeAssignment(assignment);
		}
	}

	/**
	 * Selects two periods that can be interchanged. These periods must not contain
	 * pre-assignments or double sessions. The method tries up to 99 random
	 * combinations of periods.
	 *
	 * @param timetable the timetable from which two periods should be selected.
	 * @return an array containing two timetable periods if a suitable pair was
	 *         found, otherwise {@code null}.
	 */
	private TimetablePeriod[] selectSuitablePeriods(Timetable timetable) {
		Random random = new Random();
		TimetablePeriod periodA;
		TimetablePeriod periodB;
		TimetableDay dayA;
		TimetableDay dayB;

		int counter = 0;
		do {
			dayA = timetable.getDays().get(random.nextInt(timetable.getDays().size()));
			dayB = timetable.getDays().get(random.nextInt(timetable.getDays().size()));
			periodA = dayA.getPeriods().get(random.nextInt(dayA.getPeriods().size()));
			periodB = dayB.getPeriods().get(random.nextInt(dayB.getPeriods().size()));
			counter++;

			/*
			 * Check if the periods are the same and if the periods contain double sessions
			 * or preassignments and if there wont be more than one lecture in a day.
			 */
			if (areSwapable(dayA, dayB, periodA, periodB)) {
				return new TimetablePeriod[] { periodA, periodB };
			}

		} while (counter < 100);
		// No suitable pair of periods could be found, probably too many
		// double sessions and pre-assignments
		return null;
	}

	/**
	 * Checks if two periods on two given days are swapable.<br>
	 * They are swapable if they meet the following criteria:<br>
	 * <ul>
	 * <li>The periods are not the same (same day and same timeslot)</li>
	 * <li>The periods do not contain double sessions</li>
	 * <li>The room for a preassigned session is not needed by a swapped
	 * session</li>
	 * </ul>
	 * 
	 * @param dayA    - Day of the first period.
	 * @param dayB    - Day of the second period.
	 * @param periodA - First period.
	 * @param periodB - second period.
	 * @return true if they are swapable.
	 */
	private boolean areSwapable(TimetableDay dayA, TimetableDay dayB, TimetablePeriod periodA,
			TimetablePeriod periodB) {

		// Check if they are the same period
		if (dayA.getDay() == dayB.getDay() && periodA.getTimeSlot() == periodB.getTimeSlot()) {
			return false;
		}

		// Check if there are two lectures in a day.
		if (twoCourseLecturesInDay(dayA, periodB, periodA) || twoCourseLecturesInDay(dayB, periodA, periodB)) {
			return false;
		}

		Map<String, TimetableAssignment> periodAPreassignments = new HashMap<String, TimetableAssignment>();
		Map<String, TimetableAssignment> periodBPreassignments = new HashMap<String, TimetableAssignment>();

		// Check for all preassignments in period a
		for (TimetableAssignment assgmt : periodA.getAssignments()) {
			if (assgmt.getSession().isDoubleSession()) {
				return false;
			}
			if (assgmt.getSession().getPreAssignment().isPresent()) {
				periodAPreassignments.put(assgmt.getRoom().getId(), assgmt);
			}
		}
		// Check for all preassignments in period b
		for (TimetableAssignment assgmt : periodB.getAssignments()) {
			if (assgmt.getSession().isDoubleSession()) {
				return false;
			}
			// If it is a preassignment it will not be swaped
			if (assgmt.getSession().getPreAssignment().isPresent()) {
				periodBPreassignments.put(assgmt.getRoom().getId(), assgmt);
			} else {
				// If it is not the room has to be free
				if (periodAPreassignments.containsKey(assgmt.getRoom().getId())) {
					return false;
				}
			}
		}
		// Check if the rooms in B are free for assignments from A
		for (TimetableAssignment assgmt : periodA.getAssignments()) {
			// Check if the assignment is a preassignment
			if (!assgmt.getSession().getPreAssignment().isPresent()) {
				if (periodBPreassignments.containsKey(assgmt.getRoom().getId())) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Checks for the period that should be switched over whether a second lecture
	 * of the same course is introduced in the day.
	 *
	 * @param day              the day that is checked.
	 * @param copyFromOtherDay the period that is copied over to the day.
	 * @param removeFromDay    the period that is removed from the day.
	 * @return {@code true} if a conflict was detected, otherwise {@code false}.
	 */
	private boolean twoCourseLecturesInDay(TimetableDay day, TimetablePeriod copyFromOtherDay,
			TimetablePeriod removeFromDay) {
		for (TimetablePeriod period : day.getPeriods()) {
			for (TimetableAssignment assignment : period.getAssignments()) {
				for (TimetableAssignment newAssgmt : copyFromOtherDay.getAssignments()) {
					if (period == removeFromDay && !newAssgmt.getSession().getPreAssignment().isPresent()) {
						// Conflicts with assignment that is gonna be removed
						// are irrelevant
						continue;
					} else if (assignment.getSession().isLecture() && newAssgmt.getSession().isLecture()
							&& assignment.getSession().getCourse().equals(newAssgmt.getSession().getCourse())) {
						return true;
					}
				}
			}
		}
		return false;
	}

	@Override
	public String getDescription() {
		return DESCRIPTION;
	}
	
	@Override
	public String getName() {
		return NAME;
	}
}
