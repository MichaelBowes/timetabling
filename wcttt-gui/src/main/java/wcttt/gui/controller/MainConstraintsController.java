package wcttt.gui.controller;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import javafx.application.HostServices;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import wcttt.gui.model.Model;
import wcttt.gui.model.ModelImpl;
import wcttt.gui.model.SelectedSlot;
import wcttt.lib.algorithms.WctttAlgorithmException;
import wcttt.lib.model.ConstraintType;
import wcttt.lib.model.Room;
import wcttt.lib.model.Timetable;
import wcttt.lib.model.TimetableAssignment;
import wcttt.lib.model.TimetableDay;
import wcttt.lib.model.TimetablePeriod;
import wcttt.lib.model.WctttModelException;
import wcttt.lib.util.ConstraintViolationsCalculator;
import wcttt.lib.util.TimetableUtil;
import wcttt.lib.util.Tuple;

/**
 * Controller for the swap functionality in the side menu.
 * 
 * @author Michael Bowes
 * @author Nicolas Bruch
 */

public class MainConstraintsController extends Controller {
	
	@FXML
	private Button swapButton;
	
	@FXML
	private TextArea assignmentField;
	
	@FXML
	private TextArea violationField;
	
	@FXML
	private Label penaltyChangeField;
	
	private ConstraintViolationsCalculator constraintCalculator;
	private List<SelectedSlot> selectedAssignments = new ArrayList<SelectedSlot>();
	private Timetable selectedTimetable;
	private List<Tuple<ConstraintType, String>> violations = new ArrayList<Tuple<ConstraintType, String>>();
	
	private SelectedSlot selection1;	
	private TimetableAssignment assignment1;
	private Room room1;
	private TimetableDay day1;
	private TimetablePeriod period1;

	private SelectedSlot selection2;	
	private TimetableAssignment assignment2;
	private Room room2;
	private TimetableDay day2;
	private TimetablePeriod period2;
	
	@FXML
	protected void initialize() {
		assignmentField.setEditable(false);
		violationField.setEditable(false);
		updateSwapButton();

		swapButton.setOnAction(event -> {
			if (selectedAssignments.size() == 2) {
				if (this.violations.size() == 0) {
					try {					
						if(TimetableUtil.swapTimeslots( 
								selectedTimetable, assignment1, day1, period1,
								room1, assignment2, day2, period2, room2)) {
							getModel().setChanged(true);
						}
						
						updateTimetablePenalty();
						getMainController().getTimetableTableController().refresh(day1.getDay() - 1);
						getMainController().getTimetableTableController().refresh(day2.getDay() - 1);
					} catch (WctttModelException | WctttAlgorithmException e) {
						Util.errorAlert("An error occured while swapping the assignments", e.getMessage());
					}
				}
			}
		});
	}
	
	@Override
	public void setup(Stage stage, HostServices hostServices, MainController mainController, Model model) {
		super.setup(stage, hostServices, mainController, model);
		Platform.runLater(this::updateGui);
	}

	/**
	 * Sets all needed objects for the calculation of violations and
	 * the swapping process.
	 */
	private void setSelectionData() {
		selection1 = selectedAssignments.get(0);
		assignment1 = selection1.getAssignment();
		room1 = getModel().getSemester().getInternalRooms().stream()
				.filter(r -> r.getName().equals(selection1.getRoom())).findFirst().orElse(null);
		
		day1 = selectedTimetable.getDays().stream()
				.filter(d -> TimetablePeriod.WEEK_DAY_NAMES[d.getDay() - 1].equals(selection1.getDay()))
				.findFirst().get();
		
		period1 = day1.getPeriods().stream()
				.filter(p -> p.getTimeSlot() == selection1.getPeriod()).findFirst().get();
				
		selection2 = selectedAssignments.get(1);
		assignment2 = selection2.getAssignment();
		room2 = getModel().getSemester().getInternalRooms().stream()
				.filter(r -> r.getName().equals(selection2.getRoom())).findFirst().orElse(null);
		
		day2 = selectedTimetable.getDays().stream()
				.filter(d -> TimetablePeriod.WEEK_DAY_NAMES[d.getDay() - 1].equals(selection2.getDay()))
				.findFirst().get();
		
		period2 = day2.getPeriods().stream()
				.filter(p -> p.getTimeSlot() == selection2.getPeriod()).findFirst().get();
	}
	
	/**
	 * Enables or disables the swap button depending on the requirements.
	 */
	private void updateSwapButton() {
		if (this.violations.size() > 0 || this.selectedAssignments.size() != 2) {
			swapButton.setDisable(true);
		} else {
			swapButton.setDisable(false);
		}
	}

	/**
	 * Creates the text for whenever constraints are violated.
	 * @param violations A tuple mapping a violated ConstraintType to the expressive violation message.
	 */
	public void setViolationsString(List<Tuple<ConstraintType, String>> violations) {
		String result = "";
		int counter = 0;
		for (Tuple<ConstraintType, String> violation : violations) {
			if(!result.contains(violation.y)) {
				result += violation.y;		
				if (counter < violations.size() - 1) {
					result += System.lineSeparator() + System.lineSeparator();
				}
				counter++;
			}			
		}

		violationField.setText(result);
	}

	public void clear() {
		violationField.clear();
		assignmentField.clear();
		selectedAssignments.clear();
		penaltyChangeField.setText("");
	}

	/**
	 * Updates the selected assignments in the view.
	 * @param assignment The last selected assignment in the timetable.
	 */
	private void assignmentSelected(SelectedSlot assignment) {
		if (selectedAssignments.size() == 2) {
			selectedAssignments.clear();
			selectedAssignments.add(assignment);
			violationField.clear();
		} else {
			selectedAssignments.add(assignment);
		}
		assignmentField.setText(getAssignmentText());
	}

	/**
	 * 
	 * Gets the details of the assignment in the selected slot within the timetable.
	 * 
	 * @return String containing relevant information about the selected assignment
	 */
	private String getAssignmentText() {
		String result = "";
		int counter = 0;
		for (SelectedSlot assignment : selectedAssignments) {
			result += assignment.getDay() + " - " + TimetablePeriod.TIME_SLOT_NAMES[assignment.getPeriod() - 1] + ":"
					+ System.lineSeparator();
			if (assignment.getAssignment() != null) {
				result += assignment.getAssignment().getSession().toString();
			} else {
				result += "Free Slot";
			}
			result += " - " + assignment.getRoom().toString();
			if(counter != 1) {
				result += System.lineSeparator() + System.lineSeparator();				
			}
			counter++;
		}
		return result;
	}
	
	/**
	 * Sets the text that shows the old and new penalty for comparison.
	 * @param newValue
	 */
	private void setPenaltyText(double newValue) {
		String text = "";
		text += Util.round(selectedTimetable.getSoftConstraintPenalty(), 2);
		text += " -> ";
		text += Util.round(newValue, 2);	
		penaltyChangeField.setText(text);
	}
	
	/*
	 * Updates penalty of the timetable.
	 */
	private void updateTimetablePenalty() {
		constraintCalculator = new ConstraintViolationsCalculator(getModel().getSemester());
		Timetable tt = getModel().getSelectedTimetable();
		tt.setSoftConstraintPenalty(constraintCalculator.calculateViolationsPenalty(constraintCalculator.calcTimetablePenalty(tt)));
		getMainController().getSideMenuController().getTimetablesController().update();
	}

	private void updateGui() {
		// Gets called when an assignment is selected
		getModel().addPropertyChangeListener(ModelImpl.SELECTED_ASSIGNMENT_PROPERTY, new PropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (evt.getNewValue() != null) {
					SelectedSlot assignment = (SelectedSlot) evt.getNewValue();
					assignmentSelected(assignment);
				}
				if (selectedAssignments.size() == 2) {
					setSelectionData();
					try {
						violations.clear();
						Tuple<Double, List<Tuple<ConstraintType, String>>> swapViolations = 
								TimetableUtil.getViolations(getModel().getSemester(), 
										selectedTimetable, assignment1, day1, period1,
										room1, assignment2, day2, period2, room2);
						violations.addAll(swapViolations.y);
						setViolationsString(violations);
						setPenaltyText(swapViolations.x);
					} catch (WctttModelException | WctttAlgorithmException e) {
						Util.errorAlert("An error occured while calculating the violations", e.getMessage());
						e.printStackTrace();
					}
				}
				updateSwapButton();
			}

		});
		getModel().addPropertyChangeListener(ModelImpl.SELECTED_TIMETABLE_PROPERTY, new PropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				clear();
				if (evt.getNewValue() != null) {
					Timetable timetable = (Timetable) evt.getNewValue();
					selectedTimetable = timetable;
				}
			}

		});
	}
}