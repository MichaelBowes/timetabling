package wcttt.lib.pdf;

import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import be.quodlibet.boxable.BaseTable;
import be.quodlibet.boxable.Cell;
import be.quodlibet.boxable.HorizontalAlignment;
import be.quodlibet.boxable.Row;
import wcttt.lib.model.PDFException;
import wcttt.lib.model.Period;
import wcttt.lib.model.Room;
import wcttt.lib.model.Session;
import wcttt.lib.model.Teacher;
import wcttt.lib.model.Timetable;
import wcttt.lib.model.TimetableAssignment;
import wcttt.lib.model.TimetableDay;
import wcttt.lib.model.TimetablePeriod;

/**
 * 
 * Util class that contains methods for converting wcttt data into pdf files.
 * 
 * @author Nicolas Bruch
 *   
 */
public class PDFUtil {

	private static final int HEADER_FONT_SIZE = 16;
	private static final float TOP_MARGIN = 4;
	private static final float BOTTOM_MARGIN = 4;
	private static final float TABLE_SPACING = 4;
	private static final Color TABLE_FILL_COLOR = Color.LIGHT_GRAY;
	private static final String FILE_EXTENTION = ".pdf";
	private static final PDFont HEADER_FONT = PDType1Font.HELVETICA_BOLD;
	private static final PDFont TABLE_FONT = PDType1Font.HELVETICA;
	private static final float TITLE_HEIGHT = HEADER_FONT.getFontDescriptor().getFontBoundingBox().getHeight() / 1000
			* HEADER_FONT_SIZE;
	
	private static float cellPadding = 5.0f;
	private static float fontSize = 6.0f;

	/**
	 * Creates a pdf file from a given {@link Timetable} and output.<br>
	 * The pdf will have the days as page header and the rooms as table header.
	 * 
	 * @param timetable        - Timetable to be converted.
	 * @param output           - File path to save to.
	 * @param horizontal       - Orientation of the pages.
	 * @param margin           - Space between page borders and content.
	 * @param format           - Page format.
	 * @param horizontalTables - Number of horizontal tables.
	 * @param verticalTables   - Number of vertical tables.
	 * @throws PDFException - If the file could not be created or drawn.
	 */
	public static void toPDF(Timetable timetable, String output, boolean horizontal, float margin, PDFFormat format,
			int horizontalTables, int verticalTables) throws PDFException {

		List<TableData> tables = new ArrayList<TableData>();
		// Map that contains the days with a map of rooms and their periods and sessions.
		Map<Integer, Map<Room, Map<TimetablePeriod, Session>>> data = new HashMap<Integer, Map<Room, Map<TimetablePeriod, Session>>>();
		for(TimetableDay day : timetable.getDays()) {
			Map<Room, Map<TimetablePeriod, Session>> dayMap = new HashMap<Room, Map<TimetablePeriod, Session>>();
			data.put(day.getDay(), dayMap);
			for(TimetablePeriod period : day.getPeriods()) {
				for(TimetableAssignment assignment : period.getAssignments()) {
					Room room = assignment.getRoom();
					if(dayMap.containsKey(room)) {
						dayMap.get(room).put(period, assignment.getSession());
					}else {
						Map<TimetablePeriod, Session> sessionMap = new HashMap<TimetablePeriod, Session>();
						sessionMap.put(period, assignment.getSession());
						dayMap.put(room, sessionMap);
					}
				}
			}
		}

		for (Map.Entry<Integer, Map<Room, Map<TimetablePeriod, Session>>> day : data.entrySet()) {
			for (Map.Entry<Room, Map<TimetablePeriod, Session>> room : day.getValue().entrySet()) {
				TableData tableData = new TableData(
						Period.WEEK_DAY_NAMES[day.getKey() - 1],
						room.getKey().getName());

				for (int i = 0; i < Period.TIME_SLOT_NAMES.length; i++) {
					List<String> row = new ArrayList<String>();
					String timeSlotName = Period.TIME_SLOT_NAMES[i];
					row.add(timeSlotName);
					Session currentSession = null;
					for(Map.Entry<TimetablePeriod, Session> session : room.getValue().entrySet())
						if(session.getKey().getTimeSlot() - 1 == i) {
							currentSession = session.getValue();
						}
					if (currentSession != null) {
						row.add(currentSession.getName());
					} else {
						row.add("");
					}
					tableData.addRow(row);
				}
				tables.add(tableData);
			}
		}

		createPDF(tables, output, horizontal, margin, format, horizontalTables, verticalTables);
	}
	
	public static void toPDF(Timetable timetable, Teacher teacher, String output, boolean horizontal, float margin, PDFFormat format,
			int horizontalTables, int verticalTables) throws PDFException {
		
		List<TableData> tables = new ArrayList<TableData>();
		
		Map<Integer, Map<String, Session>> data = new HashMap<Integer, Map<String, Session>>();
		
		int currentDay = 0;
		for (TimetableDay day : timetable.getDays()) {
			// Adding the day to the map
			data.put(currentDay, new HashMap<String, Session>());
			for (TimetablePeriod period : day.getPeriods()) {
				for (TimetableAssignment assignment : period.getAssignments()) {
					Map<String, Session> periods =  data.get(currentDay);
					//Check if the teacher is the given teacher
					if(assignment.getSession().getTeacher().getId().equals(teacher.getId())) {
						periods.put(period.getTimeSlotName(), assignment.getSession());
					}
				}
			}
			currentDay++;
		}
		
		for(Map.Entry<Integer, Map<String, Session>> day : data.entrySet()) {
			//Creating the new table with teacher name as page header and day as table header
			TableData table = new TableData(teacher.getName(), Period.WEEK_DAY_NAMES[day.getKey()]);
			
			for (int i = 0; i < Period.TIME_SLOT_NAMES.length; i++) {
				List<String> row = new ArrayList<String>();
				String currentTimeSlot = Period.TIME_SLOT_NAMES[i];
				row.add(currentTimeSlot);
				if(day.getValue().containsKey(currentTimeSlot)) {
					row.add(day.getValue().get(currentTimeSlot).getName());
				}else {
					row.add("");
				}
				table.addRow(row);
			}
			tables.add(table);
		}		
		createPDF(tables, output, horizontal, margin, format, horizontalTables, verticalTables);
	}

	/**
	 * Creates a pdf file for a given {@link List} of {@link TableData}.<br>
	 * A new page will be created if the page header of the {@link TableData}
	 * changes, or the tables go over the height of the given format. If the given
	 * {@link List} of {@link TableData} is not sorted by different page header will
	 * still create new pages and therefore maybe waste space.
	 * 
	 * @param tables           - Content data for the tables to be drawn.
	 * @param output           - Path for the output.
	 * @param horizontal       - Orientation of the format.
	 * @param margin           - Margin between the borders of the pages.
	 * @param format           - Format of the document.
	 * @param horizontalTables - Number of horizontal tables.
	 * @param verticalTables   - Number of vertical tables.
	 * @throws PDFException - If the file could not be created or drawn.
	 */
	private static void createPDF(List<TableData> tables, String output, boolean horizontal, float margin,
			PDFFormat format, int horizontalTables, int verticalTables) throws PDFException {

		if (tables == null) {
			throw new IllegalArgumentException("The table data can't be null");
		}
		
		if(horizontalTables >= 4 || verticalTables >=4) {
			cellPadding = 2;
		}else {
			cellPadding = 5;
		}
		float pageWidth;
		float pageHeight;
		if (horizontal) {
			pageWidth = format.getHeight();
			pageHeight = format.getWidth();
		} else {
			pageWidth = format.getWidth();
			pageHeight = format.getHeight();
		}

		// Calculating space for the tables
		float headerSize = TITLE_HEIGHT + TOP_MARGIN * 2;
		float usableHeight = pageHeight - headerSize - BOTTOM_MARGIN;
		float usableWidth = pageWidth - margin * 2;
		float tableWidth = usableWidth / (float) horizontalTables - TABLE_SPACING * ((float) horizontalTables - 1);
		float tableHeight = usableHeight / (float) verticalTables - TABLE_SPACING * ((float) verticalTables - 1);
		try (PDDocument document = new PDDocument()) {
			int horizontalCount = 0;
			int verticalCount = 0;

			String currentHeader = null;
			PDPage page = null;

			for (TableData data : tables) {
				if (currentHeader == null) {
					page = createPage(data.getPageHeader(), pageWidth, pageHeight, document);
					currentHeader = data.getPageHeader();
					document.addPage(page);
				}
				// After the max number of tables has been reached or a new page header is named
				// a new page is created.
				boolean isSameDay = currentHeader.equals(data.getPageHeader());
				if (horizontalCount == horizontalTables || !isSameDay) {
					verticalCount++;
					horizontalCount = 0;
					if (verticalCount == verticalTables || !isSameDay) {
						page = createPage(data.getPageHeader(), pageWidth, pageHeight, document);
						currentHeader = data.getPageHeader();
						document.addPage(page);
						verticalCount = 0;
					}
				}
				float xPosition = tableWidth * horizontalCount + margin + TABLE_SPACING * horizontalCount;
				float yPosition = page.getMediaBox().getHeight()
						- (headerSize + verticalCount * tableHeight + verticalCount * TABLE_SPACING);

				drawTable(page, document, data.getCellData(), tableWidth, tableHeight, xPosition, yPosition,
						data.getTableHeader());

				horizontalCount++;
			}

			// Saving the file
			if (output.endsWith(FILE_EXTENTION)) {
				document.save(output);
			} else {
				document.save(output + FILE_EXTENTION);
			}
		} catch (IOException e) {
			throw new PDFException("An error occured while trying to create the PDF file", e);
		}
	}

	/**
	 * Draws a table to a {@link PDDocument}
	 * 
	 * @param page        on which the table is to be drawn.
	 * @param document    in which the table is to be drawn.
	 * @param tableData   - data for the table content.<br>
	 *                    Each entry in the {@link List} represents a row in the
	 *                    table.<br>
	 *                    The first column of the table is composed of the first
	 *                    element of each {@link List}.<br>
	 *                    The first column has a size of 30% of the table width.<br>
	 *                    While every other cell has a size of 70% of width / (cells
	 *                    - 1).
	 * @param width       of the table in points.
	 * @param height      of the table in points.
	 * @param xPosition   of the table.
	 * @param yPosition   of the table.
	 * @param tableHeader - Header for the table.
	 * @throws IOException
	 */
	private static void drawTable(PDPage page, PDDocument document, List<List<String>> tableData, float width,
			float height, float xPosition, float yPosition, String tableHeader) throws IOException {

		if (page == null || document == null) {
			throw new IllegalArgumentException("The document or page can't be null");
		}
		if (tableData == null) {
			throw new IllegalArgumentException("Table data can't be null");
		}
		if (tableData.get(0) == null) {
			throw new IllegalArgumentException("Table data can't be null");
		}

		BaseTable table = new BaseTable(yPosition, page.getMediaBox().getHeight(), 0f, width, xPosition, document, page,
				true, true);

		float rowHeight = height / (tableData.size() + 1f);// +1 for header.
		float cellWidth = 70f / (tableData.get(0).size() - 1);// percentage width value

		Row<PDPage> headerRow = table.createRow(rowHeight);
		Cell<PDPage> cell = headerRow.createCell(100f, tableHeader);
		cell.setFont(PDType1Font.HELVETICA_BOLD);
		setCellFormat(cell);
		table.addHeaderRow(headerRow);
		boolean firstColumn = true;
		for (List<String> rowData : tableData) {
			// Creating the rows
			Row<PDPage> row = table.createRow(rowHeight);
			for (String cellData : rowData) {
				if (firstColumn) {
					cell = row.createCell(30f, cellData);
					setCellFormat(cell);
					firstColumn = false;
				} else {
					cell = row.createCell(cellWidth, cellData);
					cell.setFont(TABLE_FONT);
					cell.setFillColor(TABLE_FILL_COLOR);
					setCellFormat(cell);
				}
			}
			firstColumn = true;
		}
		table.draw();
	}
	
	private static void setCellFormat(Cell<PDPage> cell) {
		cell.setAlign(HorizontalAlignment.CENTER);
		cell.setFontSize(fontSize);
		cell.setBottomPadding(cellPadding);
		cell.setLeftPadding(cellPadding);
		cell.setRightPadding(cellPadding);
		cell.setTopPadding(cellPadding);
	}

	/**
	 * Creates a new {@link PDPage} of a given size with the given string as
	 * centered header.
	 * 
	 * @param header   text to be used for the page.
	 * @param width    of the page in pt.
	 * @param height   of the page in pt.
	 * @param document that will contain the page.
	 * @return {@link PDPage} with the given header text and size.
	 * @throws IOException
	 */
	private static PDPage createPage(String header, float width, float height, PDDocument document) throws IOException {

		// Get the width and height of the header text
		float titleWidth = HEADER_FONT.getStringWidth(header) / 1000 * HEADER_FONT_SIZE;
		float titleHeight = HEADER_FONT.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * HEADER_FONT_SIZE;

		PDPage page = new PDPage();
		page.setMediaBox(new PDRectangle(width, height));
		// Create the header text of the page
		try (PDPageContentStream contentStream = new PDPageContentStream(document, page)) {
			contentStream.beginText();
			contentStream.setFont(HEADER_FONT, HEADER_FONT_SIZE);
			contentStream.newLineAtOffset((page.getMediaBox().getWidth() - titleWidth) / 2,
					page.getMediaBox().getHeight() - TOP_MARGIN - titleHeight);
			contentStream.showText(header);
			contentStream.endText();
		}
		return page;
	}
}
