package wcttt.lib.util;

import java.util.List;

import javax.xml.bind.annotation.XmlElementWrapper;

public class ListWrapper<T> {

	
	private List<T> list;

	public ListWrapper() {};
	
	public ListWrapper(List<T> list){
		this.list = list;
	}
	
	public void setList(List<T> list) {
		this.list = list;
	}
	
	@XmlElementWrapper(name = "wrapperList")
	public List<T> getList(){
		return list;
	}
}
