package wcttt.lib.model;

/**
 * @author Nicolas Bruch
 *
 */
public class PDFException extends Exception{
	
	private static final long serialVersionUID = -8039744467269668520L;

	public PDFException() {
		super();
	}

	public PDFException(String message) {
		super(message);
	}

	public PDFException(String message, Throwable cause) {
		super(message, cause);
	}

	public PDFException(Throwable cause) {
		super(cause);
	}
}
