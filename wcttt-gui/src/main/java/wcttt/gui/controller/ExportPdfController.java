package wcttt.gui.controller;

import java.io.File;
import java.util.Optional;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import wcttt.lib.model.PDFException;
import wcttt.lib.model.Teacher;
import wcttt.lib.model.Timetable;
import wcttt.lib.pdf.PDFFormat;
import wcttt.lib.pdf.PDFUtil;

/**
 * 
 * Controller for the window that handles the export function for converting a timetable's data to
 * the Portable Document Format (PDF).
 * 
 * @author Nicolas Bruch
 *
 */
public class ExportPdfController extends Controller {

	private static final String ALL_ROOM_EXPORT = "All rooms";
	private static final String SPECIFIC_TEACHER_EXPORT = "Specific Teacher";
	private static final ObservableList<Integer> tableCount = FXCollections.observableArrayList();
	
	private SimpleStringProperty path = new SimpleStringProperty();
	private SimpleObjectProperty<Teacher> selectedTeacher = new SimpleObjectProperty<Teacher>();
	
	@FXML
	private ComboBox<Timetable> timetableBox;
	@FXML
	private ComboBox<String> exportTypeBox;
	@FXML
	private VBox teacherSelectionField;
	@FXML
	private ComboBox<Teacher> teacherBox;
	@FXML
	private CheckBox horizontalBox;
	@FXML
	private ComboBox<PDFFormat> formatBox;
	@FXML
	private TextField marginBox;
	@FXML
	private ComboBox<Integer> verticalTablesBox;
	@FXML
	private ComboBox<Integer> horizontalTablesBox;
	@FXML
	private TextField pathText;
	@FXML
	private Button pathButton;
	@FXML
	private Button exportButton;

	@FXML
	protected void initialize() {
		formatBox.getItems().setAll(PDFFormat.values());
		formatBox.getSelectionModel().select(PDFFormat.A4);		
		
		horizontalBox.setSelected(true);
		
		exportTypeBox.getItems().setAll(ALL_ROOM_EXPORT, SPECIFIC_TEACHER_EXPORT);
		exportTypeBox.getSelectionModel().select(0);
		teacherSelectionField.setVisible(false);
		
		tableCount.addAll(1,2,3,4);
		verticalTablesBox.setItems(tableCount);
		horizontalTablesBox.setItems(tableCount);
		verticalTablesBox.getSelectionModel().select(3);
		horizontalTablesBox.getSelectionModel().select(3);

		exportTypeBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if(newValue.equals(SPECIFIC_TEACHER_EXPORT)) {
					showTeacherSelection(true);
				}else {
					showTeacherSelection(false);
				}
			}
		});

		pathText.textProperty().bind(path);
		selectedTeacher.bind(teacherBox.getSelectionModel().selectedItemProperty());

		pathButton.setOnAction(event -> {
			Optional<File> file = Util.chooseFileToSaveDialog(getStage().getScene().getWindow(), "pdf");
			if (file.isPresent()) {
				path.setValue(file.get().getPath());
			}
		});

		exportButton.setOnAction(event -> {
			float margin = 0;
			int horizontalTables =horizontalTablesBox.getSelectionModel().getSelectedItem();
			int verticalTables = verticalTablesBox.getSelectionModel().getSelectedItem();
			Timetable timetable = timetableBox.getSelectionModel().getSelectedItem();
			
			if (timetable == null) {
				Util.informationAlert("Could not export", "Please select a timetable");
				return;
			}
			try {
				margin = Float.parseFloat(marginBox.getText());
			} catch (NumberFormatException e) {
				Util.errorAlert("Could not export", "Invalid value(s) were entered");
				return;
			}

			try {
				switch (exportTypeBox.getSelectionModel().getSelectedItem()) {
				case ALL_ROOM_EXPORT:
					PDFUtil.toPDF(timetable, pathText.getText(), horizontalBox.selectedProperty().getValue(), margin,
							formatBox.getSelectionModel().getSelectedItem(), horizontalTables, verticalTables);
					break;
					
				case SPECIFIC_TEACHER_EXPORT:
					if(selectedTeacher.getValue() == null) {
						Util.informationAlert("Could not export", "Please select a teacher");
						return;
					}
					PDFUtil.toPDF(timetable, selectedTeacher.getValue(), pathText.getText(),
							horizontalBox.selectedProperty().getValue(), margin,
							formatBox.getSelectionModel().getSelectedItem(), horizontalTables, verticalTables);
					break;
				}
				Util.informationAlert("Success", "The pdf file was successfully created.");
				getStage().close();
			} catch (PDFException e) {
				Util.exceptionAlert(e);
			}
		});
		timetableBox.setOnMouseClicked(event -> {
			timetableBox.getItems().setAll(getModel().getTimetables());
		});
	}

	private void showTeacherSelection(boolean visible) {
		if (visible) {
			teacherSelectionField.setVisible(true);
			teacherBox.getItems().setAll(getModel().getTeachers());
		} else {
			teacherSelectionField.setVisible(false);
			teacherBox.getSelectionModel().select(null);
		}
	}
}
